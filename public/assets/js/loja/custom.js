$(function () {
    $('.btn-add-produto').on('click', function () {
        let produto = $(this).attr('data-produto');
        $.ajax({
            type: 'GET',
            url: 'carrinho/add/'+produto,
            beforeSend: function() {

            },
            success: function(data) {
                showNotification('bg-blue', 'Adicionado ao carrinho');
            },
            error: function(xhr) {
                showNotification('bg-red', 'Erro ao adicionar produto');
            }
        });
    })

    $('.btn-menos').on('click', function () {
        let id = $(this).attr('data-id');
        let campoQtd = $('#qtd-'+id);
        let qtd = parseInt(campoQtd.val()) - 1;
        let carrinho = campoQtd.attr('data-carrinho');
        if (campoQtd.val() > 1){
            campoQtd.val(qtd);
            atualizarCarrinho(carrinho, qtd, campoQtd, $(this),true);
        }
    });

    $('.btn-mais').on('click', function () {
        let id = $(this).attr('data-id');
        let campoQtd = $('#qtd-'+id);
        let qtd = parseInt(campoQtd.val()) + 1;
        let carrinho = campoQtd.attr('data-carrinho');
        campoQtd.val(qtd);

        atualizarCarrinho(carrinho, qtd, campoQtd, $(this),true);
    });
});

function atualizarCarrinho(id, qtd, campo, button, isPlus) {
    $.ajax({
        type: 'GET',
        url: 'update/'+ id + '/' + qtd,
        beforeSend: function() {
            button.prop('disabled', true);
        },
        success: function(data) {
            $('#total-'+id).html('R$ ' + data.total_produto);
            $('#sub-total-carrinho').html('R$ ' + data.total_carrinho)
        },
        error: function(xhr) {
            if (isPlus) {
                campo.val(qtd - 1);
                return;
            }
            campo.val(qtd + 1);
        },
        complete: function () {
            button.prop('disabled', false);
        }
    });
}