$(function () {
    $('#form_produto').validate({
        rules: {
            'produto[unidadeMedida]': {
                lettersonly: true
            },
            'produto[gtin]': {
                digits: true
            }
        }
    });

    $('#produto_tipo').on('change', function(){

        if ($(this).val() == 1) {
            getCamposProduto();
        } else {
            getCamposServico();
        }
    });
    
    function getCamposProduto() {
        $('.field-produto').show();
    }
    
    function getCamposServico() {
        $('.field-produto').hide();
    }
});