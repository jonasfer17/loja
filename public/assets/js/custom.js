$(document).ready(function(){
    var rowDeleted = null;

    // Mask
    $('.date').mask('00/00/0000');
    $('.cep').mask('00000-000');
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.decimal').mask("#.##0,00", {reverse: true});

    // Modal
    $('.btn-delete').on('click', function() {
        let url = $(this).attr('data-url');
        let label = $(this).attr('data-label');
        rowDeleted =  $(this).closest('tr');
        $('#modal-delete-label').html(label);
        $('#btn-confirm-delete').attr('data-url', url);
        $('#modal-delete').modal();
    });

    $('#btn-confirm-delete').on('click', function() {
        let urlDelete = $(this).attr('data-url');
        $.ajax({
            url : urlDelete,
            type : 'DELETE',
            beforeSend : function(){
                $("#modal-delete-body").html(getPreload);
            }
       })
       .success(function(response) {
            $("#modal-delete").modal('toggle');
            rowDeleted.remove();
            getModalDefault(response);
       })
       .fail(function(response) {
            $("#modal-delete").modal('toggle');
            getModalDefault(JSON.parse(response.responseText));
       })
       .complete(function(response) {
            resetModalDelete();
       }); 
    });

 
    function getPreload() {
        return "<div class='align-center'> "
            + "<div class='preloader pl-size-xl'>"
            + "<div class='spinner-layer'>"
            + "<div class='circle-clipper left'>"
            + "<div class='circle'>"
            + "</div></div>"
            + "<div class='circle-clipper right'>"
            + "<div class='circle'></div></div>"
            + "</div></div></div>";
    }

    function getModalDefault(mensagem) {
        $("#modal-body").html(mensagem);
        $("#modal-default").modal();
    }

    function resetModalDelete() {
        $("#modal-delete-body").html('Remover registro?');
    }
      
    $('.form-line').removeClass('focused');

    // Validator default
    jQuery.extend(jQuery.validator.messages, {
        required: "Campo obrigatório.",
        remote: "Por favor, corrija este campo.",
        email: "Por favor, forneça um email válido.",
        url: "Por favor, forneça uma URL válida.",
        date: "Por favor, forneça uma data válida.",
        dateISO: "Por favor, forneça uma data válida (ISO).",
        number: "Por favor, forneça somente números.",
        digits: "Por favor, forneça somente dígitos.",
        creditcard: "Por favor, forneça um cartão de crédito válido.",
        equalTo: "Por favor, forneça o mesmo valor novamente.",
        accept: "Por favor, forneça um valor com uma extensão válida.",
        maxlength: jQuery.validator.format("Por favor, forneça não mais que {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, forneça ao menos {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, forneça um valor entre {0} e {1} caracteres de comprimento."),
        range: jQuery.validator.format("Por favor, forneça um valor entre {0} e {1}."),
        max: jQuery.validator.format("Por favor, forneça um valor menor ou igual a {0}."),
        min: jQuery.validator.format("Por favor, forneça um valor maior ou igual a {0}.")
    });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Por favor informe somente letras.");

    jQuery.validator.setDefaults({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-line').append(error);
        }
    });
});