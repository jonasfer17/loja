<?php

namespace App\Infrastructure\Assembler\Financeiro;

use App\Domain\Dto\MovimentacaoDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Empresa;
use App\Domain\Model\Financeiro\Movimentacao;
use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Usuario;
use App\Infrastructure\Doctrine\Repository\Financeiro\MovimentacaoRepository;
use App\Infrastructure\Utils\FloatConverter;

/**
 * Class MovimentacaoAssembler
 * @package App\Infrastructure\Assembler\Financeiro
 */
class MovimentacaoAssembler
{
    /**
     * @var MovimentacaoRepository
     */
    private $movimentacaoRepository;

    /**
     * MovimentacaoAssembler constructor.
     * @param MovimentacaoRepository $movimentacaoRepository
     */
    public function __construct(MovimentacaoRepository $movimentacaoRepository)
    {
        $this->movimentacaoRepository = $movimentacaoRepository;
    }

    /**
     * @param MovimentacaoDto $movimentacaoDto
     * @param Produto $produto
     * @param Usuario $usuario
     * @param string|null $descricao
     * @param Movimentacao|null $movimentacao
     * @return Movimentacao
     */
    public function readDto(
        MovimentacaoDto $movimentacaoDto,
        Produto $produto,
        Usuario $usuario,
        ?string $descricao,
        ?Movimentacao $movimentacao = null
    ): Movimentacao {
        if (!$movimentacao) {
            $movimentacao = new Movimentacao();
            $movimentacao->createdAt();
        }

        $movimentacao->setProduto($produto);
        $movimentacao->setUsuario($usuario);
        $movimentacao->setValorCompra($movimentacaoDto->getValorCusto());
        $movimentacao->setValorVenda($movimentacaoDto->getValorVenda());
        $movimentacao->setQuantidade($movimentacaoDto->getQuantidade() ?? 0);
        $movimentacao->setAjuste($movimentacao->getQuantidade() + $this->getQuantidadeAtual($produto));
        $movimentacao->setDescricao($descricao);

        return $movimentacao;
    }

    /**
     * @param MovimentacaoDto $movimentacaoDto
     * @param Produto $produto
     * @param Usuario $usuario
     * @param string|null $descricao
     * @return Movimentacao
     */
    public function create(
        MovimentacaoDto $movimentacaoDto,
        Produto $produto,
        Usuario $usuario,
        ?string $descricao
    ): Movimentacao {
        return $this->readDto($movimentacaoDto, $produto, $usuario, $descricao);
    }

    /**
     * @param Produto $produto
     * @return float
     */
    private function getQuantidadeAtual(Produto $produto): float
    {
        return $this->movimentacaoRepository->getLastAjuste($produto) ?? 0;
    }

    /**
     * @param Movimentacao $movimentacao
     * @return MovimentacaoDto
     */
    public function writeDto(Movimentacao $movimentacao): MovimentacaoDto
    {
        $movimentacaoDto = new MovimentacaoDto();

        $movimentacaoDto->setQuantidade(FloatConverter::revert($movimentacao->getAjuste()));
        $movimentacaoDto->setValorVenda(FloatConverter::revert($movimentacao->getValorVenda()));
        $movimentacaoDto->setValorCusto(FloatConverter::revert($movimentacao->getValorCompra()));

        return $movimentacaoDto;
    }
}