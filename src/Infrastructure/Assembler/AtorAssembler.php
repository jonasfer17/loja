<?php

namespace App\Infrastructure\Assembler;

use App\Domain\Dto\PessoaDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Contato;

/**
 * Class AtorAssembler
 */
final class AtorAssembler
{
    /**
     * @var EnderecoAssembler
     */
    private $enderecoAssembler;

    /**
     * @var ContatoAssembler
     */
    private $contatoAssembler;

    public function __construct(
        EnderecoAssembler $enderecoAssembler,
        ContatoAssembler $contatoAssembler
    ) {
        $this->enderecoAssembler = $enderecoAssembler;
        $this->contatoAssembler = $contatoAssembler;   
    }

    /**
     * @param PessoaDto $pessoaDto
     * @param Ator|null $pessoa
     * 
     * @return Ator
     */
    public function readDTO(
        PessoaDto $pessoaDto, 
        ?Ator $pessoa = null
    ): Ator {
        if (!$pessoa) {
            $pessoa = new Ator();
            $pessoa->createdAt();
        }

        $contato = $pessoa->getContato()->isEmpty() ? null : $pessoa->getContato()->first() ;
        $endereco = $pessoa->getEndereco()->isEmpty() ? null : $pessoa->getEndereco()->first();

        $pessoa->setNome($pessoaDto->getNome());
        $pessoa->setTipo($pessoaDto->getTipo());
        $pessoa->setCpfCnpj($pessoaDto->getCpfCnpj());
        $pessoa->setContato($this->contatoAssembler->readDTO($pessoaDto->getContato(), $pessoa, $contato));
        $pessoa->setEndereco($this->enderecoAssembler->readDTO($pessoaDto->getEndereco(), $pessoa, $endereco));
        $pessoa->updateAt();

        return $pessoa;
    }

    /**
     * @param Pessoa $pessoa
     * @param PessoaDto $pessoaDto
     * @return Contato
     */
    public function updatePessoa(
        Ator $pessoa, 
        PessoaDto $pessoaDto
    ): Ator {
        return $this->readDTO($pessoaDto, $pessoa);
    }
    
    /**
     * @param PessoaDto $pessoaDto
     * 
     * @return Pessoa
     */
    public function createPessoa(PessoaDto $pessoaDto): Ator
    {
        return $this->readDTO($pessoaDto);
    }

    /**
     * @param array $pessoa
     * @return PessoaDto
     */
    public function writeDTO(array $pessoa): PessoaDto
    {
        if (!$pessoa) {
            return new PessoaDto();
        }

        return new PessoaDto(
            $pessoa['tipo'],
            $pessoa['nome'],
            $pessoa['cpfCnpj'],
            $this->contatoAssembler->writeDTO($pessoa),
            $this->enderecoAssembler->writeDTO($pessoa)
        );
    }
}