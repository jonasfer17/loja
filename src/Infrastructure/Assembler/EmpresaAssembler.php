<?php

namespace App\Infrastructure\Assembler;

use App\Domain\Dto\EmpresaDto;
use App\Domain\Model\Empresa;

/**
 * Class EmpresaAssembler
 */
class EmpresaAssembler
{
    /**
     * @var AtorAssembler
     */
    private $atorAssembler;

    /**
     * @param AtorAssembler $atorAssembler
     * 
     * @return void
     */
    public function __construct(AtorAssembler $atorAssembler)
    {
        $this->atorAssembler = $atorAssembler;    
    }
    /**
     * @param EmpresaDto $empresaDto
     * @param Empresa|null $empresa
     * 
     * @return Empresa
     */
    public function readDTO(EmpresaDto $empresaDto, ?Empresa $empresa = null): Empresa
    {
        if (!$empresa) {
            $empresa = new Empresa();
        }

        return $empresa;
    }

    /**
     * @param Empresa $empresa
     * @param EmpresaDto $empresaDto
     * 
     * @return Empresa
     */
    public function updateEmpresa(Empresa $empresa, EmpresaDto $empresaDto): Empresa
    {
        return $this->readDTO($empresaDto, $empresa);
    }
    
    /**
     * @param EmpresaDto $empresaDto
     * 
     * @return Empresa
     */
    public function createEmpresa(EmpresaDto $empresaDto): Empresa
    {
        return $this->readDTO($empresaDto);
    }

    /**
     * @param array $empresa
     * 
     * @return EmpresaDto
     */
    public function writeDTO(array $empresa): EmpresaDto
    {
        if (!$empresa) {
            return new EmpresaDto();
        }

        return new EmpresaDto(
            $this->atorAssembler->writeDTO($empresa)
        );
    }
}