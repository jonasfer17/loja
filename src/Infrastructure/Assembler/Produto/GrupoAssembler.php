<?php

namespace App\Infrastructure\Assembler\Produto;

use App\Domain\Dto\GrupoProdutoDto;
use App\Domain\Model\Produto\Grupo;
use App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository;

/**
 * Class GrupoAssembler
 * @package App\Infrastructure\Assembler\Produto
 */
class GrupoAssembler
{
    /**
     * @var GrupoRepository
     */
    private $grupoRepository;

    /**
     * GrupoAssembler constructor.
     * @param GrupoRepository $grupoRepository
     */
    public function __construct(GrupoRepository $grupoRepository)
    {
        $this->grupoRepository = $grupoRepository;
    }

    /**
     * @param GrupoProdutoDto $grupoProdutoDto
     * @param Grupo|null $grupo
     * @return Grupo
     */
    public function readDto(
        GrupoProdutoDto $grupoProdutoDto,
        ?Grupo $grupo = null
    ): Grupo {
        if (!$grupo) {
            $grupo = new Grupo();
        }
        $grupoPai = $grupoProdutoDto->getGrupoPai()
            ? $this->grupoRepository->find($grupoProdutoDto->getGrupoPai())
            : null;
        $grupo->setNome($grupoProdutoDto->getNome());
        $grupo->setGrupoPai($grupoPai);

        $grupo->createdAt();

        return $grupo;
    }

    /**
     * @param Grupo $grupo
     * @return GrupoProdutoDto
     */
    public function writeDTO(Grupo $grupo): GrupoProdutoDto
    {
        if (!$grupo) {
            return new GrupoProdutoDto();
        }

        $grupoDto = new GrupoProdutoDto();
        $grupoDto->setNome($grupo->getNome());

        if ($grupo->getGrupoPai()) {
            $grupoDto->setGrupoPai($grupo->getGrupoPai()->getId());
        }

        return $grupoDto;
    }
}