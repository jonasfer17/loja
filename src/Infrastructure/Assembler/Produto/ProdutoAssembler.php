<?php

namespace App\Infrastructure\Assembler\Produto;

use App\Domain\Dto\MovimentacaoDto;
use App\Domain\Dto\ProdutoDto;
use App\Domain\Model\Financeiro\Movimentacao;
use App\Domain\Model\Produto\Colecao;
use App\Domain\Model\Produto\Grupo;
use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Produto\Raridade;
use App\Infrastructure\Assembler\Financeiro\MovimentacaoAssembler;
use App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository;
use App\Infrastructure\Service\EmpresaService;
use App\Infrastructure\Utils\FloatConverter;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ProdutoAssembler
 * @package App\Infrastructure\Assembler\Produto
 */
class ProdutoAssembler
{
    /**
     * @var \App\Domain\Model\Ator
     */
    private $ator;

    /**
     * @var MovimentacaoAssembler
     */
    private $movimentacaoAssembler;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ProdutoAssembler constructor.
     * @param EmpresaService $empresaService
     * @param MovimentacaoAssembler $movimentacaoAssembler
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EmpresaService $empresaService,
        MovimentacaoAssembler $movimentacaoAssembler,
        EntityManagerInterface $entityManager
    ) {
        $this->ator = $empresaService->getEmpresa();
        $this->movimentacaoAssembler = $movimentacaoAssembler;
        $this->entityManager = $entityManager;
    }

    public function readDto(
        ProdutoDto $produtoDto,
        ?Produto $produto = null
    ) {
        if (!$produto) {
            $produto = new Produto();
            $produto->createdAt();
        }

        $produto->setNome($produtoDto->getNome());
        $produto->setIdioma($produtoDto->getIdioma());
        $produto->setGrupo(
            $this->entityManager->getRepository(Grupo::class)->find($produtoDto->getGrupo())
        );
        $produto->setRaridade(
            $this->entityManager->getRepository(Raridade::class)->find($produtoDto->getRaridade())
        );
        $produto->setColecao(
            $this->entityManager->getRepository(Colecao::class)->find($produtoDto->getColecao())
        );
        $produto->setDescricao($produtoDto->getDescricao());
        $produto->setValorVenda($produtoDto->getMovimentacao()->getValorVenda());
        $produto->setValorCompra($produtoDto->getMovimentacao()->getValorCusto());

        return $produto;
    }

    /**
     * @param ProdutoDto $produtoDto
     * @return Produto
     */
    public function create(ProdutoDto $produtoDto): Produto
    {
        return $this->readDto($produtoDto);
    }

    /**
     * @param Produto $produto
     * @param Movimentacao $movimentacao
     * @return ProdutoDto
     */
    public function writeDto(Produto $produto, Movimentacao $movimentacao): ProdutoDto
    {
        $produtoDto = new ProdutoDto();
        $produtoDto->setNome($produto->getNome());
        $produtoDto->setGrupo($produto->getGrupo() ? $produto->getGrupo()->getId() : 0);
        $produtoDto->setColecao($produto->getColecao() ? $produto->getColecao()->getId() : 0);
        $produtoDto->setRaridade($produto->getRaridade() ? $produto->getRaridade()->getId() : 0);
        $produtoDto->setIdioma($produto->getIdioma());
        $produtoDto->setDescricao($produto->getDescricao());
        $produtoDto->setMovimentacao($this->movimentacaoAssembler->writeDto($movimentacao));

        return $produtoDto;
    }
}