<?php

namespace App\Infrastructure\Assembler;

use App\Domain\Dto\EnderecoDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Endereco;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class EnderecoAssembler
 */
final class EnderecoAssembler
{
    /**
     * @param EnderecoDto $enderecoDto
     * @param ?Endereco $endereco
     * 
     * @return Endereco
     */
    public function readDTO(
        EnderecoDto $enderecoDto,
        Ator $ator,
        ?Endereco $endereco = null
    ): ArrayCollection {
        if (!$endereco) {
            $endereco = new Endereco();
            $endereco->createdAt();
            $endereco->setAtor($ator);
        }
        $endereco->setRua($enderecoDto->getRua());
        $endereco->setNumero($enderecoDto->getNumero());
        $endereco->setCep($enderecoDto->getCep());
        $endereco->setBairro($enderecoDto->getBairro());
        $endereco->setMunicipio($enderecoDto->getMunicipio());
        $endereco->setEstado($enderecoDto->getEstado());
        $endereco->updateAt();
        
        return new ArrayCollection([$endereco]);
    }

    /**
     * @param Endereco $endereco
     * @param EnderecoDto $enderecoDto
     * 
     * @return Endereco
     */
    public function updateEndereco(
        Endereco $endereco, 
        EnderecoDto $enderecoDto
    ): Endereco {
        return $this->readDTO($enderecoDto, $endereco);
    }
    
    /**
     * @param EnderecoDto $enderecoDto
     * 
     * @return Endereco
     */
    public function createEmpresa(EnderecoDto $enderecoDto): Endereco
    {
        return $this->readDTO($enderecoDto);
    }

    /**
     * @param array $endereco
     * 
     * @return EnderecoDto
     */
    public function writeDTO(array $endereco): EnderecoDto
    {
        if (!$endereco) {
            return new EnderecoDto();
        }

        return new EnderecoDto(
            $endereco['rua'],
            $endereco['numero'],
            $endereco['cep'],
            $endereco['bairro'],
            $endereco['municipio'],
            $endereco['estado']
        );
    }
}