<?php

namespace App\Infrastructure\Assembler;

use App\Domain\Dto\ContatoDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Contato;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ContatoAssembler
 */
final class ContatoAssembler
{
    /**
     * @param ContatoDto $contatoDto
     * @param Ator $ator
     * @param Contato|null $contato
     * @return ArrayCollection
     */
    public function readDTO(
        ContatoDto $contatoDto,
        Ator $ator,
        ?Contato $contato = null
    ): ArrayCollection {
        if (!$contato) {
            $contato = new Contato();
            $contato->setAtor($ator);
            $contato->createdAt();
        }

        $contato->setEmail($contatoDto->getEmail());
        $contato->setTelefone($contatoDto->getTelefone());
        $contato->setCelular($contatoDto->getCelular());
        $contato->updateAt();
        
        return new ArrayCollection([$contato]);
    }

    /**
     * @param Contato $contato
     * @param ContatoDto $contatoDto
     * @return Contato
     */
    public function updateContato(
        Contato $contato, 
        ContatoDto $contatoDto
    ): Contato {
        return $this->readDTO($contatoDto, $contato);
    }
    
    /**
     * @param ContatoDto $contatoDto
     * 
     * @return Contato
     */
    public function createContato(ContatoDto $contatoDto): Contato
    {
        return $this->readDTO($contatoDto);
    }

    /**
     * @param array $contato
     * @return ContatoDto
     */
    public function writeDTO(array $contato): ContatoDto
    {
        if (!$contato) {
            return new ContatoDto();
        }

        return new ContatoDto(
            $contato['email'],
            $contato['telefone'],
            $contato['celular']
        );
    }
}