<?php

namespace App\Infrastructure\Service;

use App\Domain\Model\Produto\Colecao;
use App\Domain\Model\Produto\Raridade;
use App\Infrastructure\Doctrine\Repository\Produto\RaridadeRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class RaridadeService
 * @package App\Infrastructure\Service
 */
class RaridadeService
{
    /**
     * @var RaridadeRepository
     */
    private $raridadeRepository;

    /**
     * RaridadeService constructor.
     * @param RaridadeRepository $raridadeRepository
     */
    public function __construct(RaridadeRepository $raridadeRepository)
    {
        $this->raridadeRepository = $raridadeRepository;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAll(): array
    {
        return $this->raridadeRepository->findAll();
    }

    /**
     * @param int $id
     * @return Raridade
     */
    public function find(int $id): Raridade
    {
        $raridade = $this->raridadeRepository->findOneBy([
            'id' => $id
        ]);

        if (!$raridade) {
            throw new \DomainException(Men::MENSAGEM_ERRO_NAO_AUTORIZADO);
        }

        return $raridade;
    }

    /**
     * @param Raridade $raridade
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(Raridade $raridade): void
    {
        if ($this->hasRaridadeMesmoNome($raridade)) {
            throw new \DomainException('Raridade com mesmo nome cadastrado!');
        }
        $raridade->createdAt();

        $this->raridadeRepository->salvar($raridade);
    }

    /**
     * @param Raridade $raridade
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Raridade $raridade): void
    {
        if ($this->hasRaridadeMesmoNome($raridade)) {
            throw new \DomainException('Coleção com mesmo nome cadastrada!');
        }
        $raridade->updateAt();

        $this->raridadeRepository->salvar($raridade);
    }

    /**
     * @param Raridade $raridade
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Raridade $raridade)
    {
        $this->raridadeRepository->delete($raridade);
    }

    /**
     * @param Raridade $raridade
     * @return bool
     */
    private function hasRaridadeMesmoNome(Raridade $raridade): bool
    {
        $raridadeBd = $this->raridadeRepository
            ->findOneBy(
                [
                    'nome' => $raridade->getNome()
                ]
            );

        return $raridadeBd && $raridadeBd->getId() != $raridade->getId();
    }
}