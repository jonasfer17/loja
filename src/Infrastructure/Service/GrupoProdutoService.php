<?php

namespace App\Infrastructure\Service;

use App\Domain\Dto\GrupoProdutoDto;
use App\Domain\Model\Produto\Grupo;
use App\Infrastructure\Assembler\Produto\GrupoAssembler;
use App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Service\EmpresaService;
use App\Infrastructure\Validator\GrupoProdutoValidator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class GrupoProdutoService
 * @package App\Infrastructure\Service
 */
class GrupoProdutoService
{
    /**
     * @var GrupoRepository
     */
    private $grupoRepository;

    /**
     * @var GrupoAssembler
     */
    private $grupoAssembler;

    /**
     * @var GrupoProdutoValidator
     */
    private $grupoProdutoValidator;

    /**
     * GrupoProdutoService constructor.
     * @param GrupoRepository $grupoRepository
     * @param GrupoAssembler $grupoAssembler
     * @param GrupoProdutoValidator $grupoProdutoValidator
     */
    public function __construct(
        GrupoRepository $grupoRepository,
        GrupoAssembler $grupoAssembler,
        GrupoProdutoValidator $grupoProdutoValidator
    ) {
        $this->grupoRepository = $grupoRepository;
        $this->grupoAssembler = $grupoAssembler;
        $this->grupoProdutoValidator = $grupoProdutoValidator;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAll(): array
    {
        return $this->grupoRepository->findAll();
    }

    /**
     * @param int $id
     * @return GrupoProdutoDto
     */
    public function find(int $id): GrupoProdutoDto
    {
        $grupo = $this->grupoRepository->findOneBy([
            'id' => $id
        ]);

        if (!$grupo) {
            throw new \DomainException(MensagemEnum::MENSAGEM_ERRO_NAO_AUTORIZADO);
        }

        return $this->grupoAssembler->writeDTO($grupo);
    }

    /**
     * @param GrupoProdutoDto $grupoDto
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(GrupoProdutoDto $grupoDto): void
    {
        $this->grupoProdutoValidator->validate($grupoDto);
        $grupo = $this->grupoAssembler->readDto($grupoDto);

        $this->grupoRepository->salvar($grupo);
    }

    /**
     * @param Grupo $grupo
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(GrupoProdutoDto $grupoProdutoDto, Grupo $grupo): void
    {
        $this->grupoProdutoValidator->validate($grupoProdutoDto, $grupo);
        $this->grupoAssembler->readDto($grupoProdutoDto, $grupo);

        $this->grupoRepository->salvar($grupo);
    }

    /**
     * @param Grupo $grupo
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Grupo $grupo)
    {
        $this->grupoRepository->delete($grupo);
    }
}