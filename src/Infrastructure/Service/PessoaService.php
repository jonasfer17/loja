<?php

namespace App\Infrastructure\Service;

use App\Domain\Dto\PessoaDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Empresa;
use App\Infrastructure\Assembler\AtorAssembler;
use App\Infrastructure\Assembler\PessoaAssembler;
use App\Infrastructure\Doctrine\Repository\AtorRepository;
use Proxies\__CG__\App\Domain\Model\Pessoa;

/**
 * Class PessoaService
 */
class PessoaService 
{
    /**
     * @var $atorRepository
     */
    private $atorRepository;

    /**
     * @var AtorAssembler
     */
    private $atorAssembler;
    
    /**
     * @param AtorRepository $atorRepository
     * @param AtorAssembler $atorAssembler
     */
    public function __construct(
        AtorRepository $atorRepository,
        AtorAssembler $atorAssembler
    ) {
        $this->atorRepository = $atorRepository;
        $this->atorAssembler = $atorAssembler;
    }

    /**
     * @param PessoaDto $pessoaDto
     * @param Empresa $empresa
     * 
     * @return void
     */
    public function save(PessoaDto $pessoaDto, Empresa $empresa): void
    {
        $pessoa = $this->atorAssembler->updatePessoa($empresa->getAtor(), $pessoaDto);

        $pessoa->getId()
            ? $pessoa->updateAt()
            : $pessoa->createdAt();
        $this->atorRepository->save($pessoa);
    }

    public function update(Ator $ator, PessoaDto $pessoaDto)
    {
        $pessoa = $this->atorAssembler->updatePessoa($ator, $pessoaDto);
        $pessoa->updateAt();

        $this->atorRepository->save($pessoa);
    }
}