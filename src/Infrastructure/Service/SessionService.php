<?php


namespace App\Infrastructure\Service;

use phpDocumentor\Reflection\Types\Self_;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class SessionService
 * @package App\Infrastructure\Service
 */
class SessionService
{
    public const CARRINHO_SESSION = 'CARRINHO_SESSION';

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * SessionService constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param string $idSessao
     * @param null $valor
     */
    public function createSession(string $idSessao, $valor = null)
    {
        return $this->session->set($idSessao, $valor);
    }

    /**
     * @param string $idSessao
     * @param null $valor
     */
    public function updateSession(string $idSessao, $valor = null): void
    {
        if ($this->session->has($idSessao)) {
            $this->session->set($idSessao, $valor);
        }
    }

    /**
     * @param string $idSessao
     * @return mixed|null
     */
    public function getSession(string $idSessao)
    {
        return $this->session->has($idSessao)
            ? $this->session->get($idSessao)
            : $this->createSession($idSessao);
    }

    public function removeSession(string $idSessao)
    {
        if ($this->session->has($idSessao)) {
            $this->session->remove($idSessao);
        }
    }
}