<?php

namespace App\Infrastructure\Service;

use App\Domain\Model\Financeiro\Venda;
use App\Domain\Model\Loja\Carrinho;
use App\Domain\Model\Loja\CarrinhoProduto;
use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Usuario;
use App\Infrastructure\Doctrine\Repository\Loja\CarrinhoProdutoRepository;
use App\Infrastructure\Doctrine\Repository\Loja\CarrinhoRepository;
use App\Infrastructure\Enum\StatusCarrinhoEnum;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CarrinhoService
 * @package App\Infrastructure\Service
 */
class CarrinhoService
{
    /**
     * @var CarrinhoRepository
     */
    private $carrinhoRepository;

    /**
     * @var SessionService
     */
    private $sessionService;

    /**
     * @var Usuario
     */
    private $usuarioSession;

    /**
     * @var CarrinhoProdutoRepository
     */
    private $carrinhoProdutoRepository;

    /**
     * @var VendaService
     */
    private $vendaService;

    /**
     * CarrinhoService constructor.
     * @param CarrinhoRepository $carrinhoRepository
     * @param TokenStorageInterface $tokenStorage
     * @param SessionService $sessionService
     * @param CarrinhoProdutoRepository $carrinhoProdutoRepository
     * @param VendaService $vendaService
     */
    public function __construct(
        CarrinhoRepository $carrinhoRepository,
        TokenStorageInterface $tokenStorage,
        SessionService $sessionService,
        CarrinhoProdutoRepository $carrinhoProdutoRepository,
        VendaService $vendaService
    ) {
        $this->carrinhoRepository = $carrinhoRepository;
        $this->usuarioSession = $tokenStorage->getToken()->getUser();
        $this->sessionService = $sessionService;
        $this->carrinhoProdutoRepository = $carrinhoProdutoRepository;
        $this->vendaService = $vendaService;
    }

    /**
     * @param Produto $produto
     * @param UserInterface|null $usuario
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Produto $produto, ?UserInterface $usuario = null)
    {
        $carrinho = $this->getCarrinhoAberto();

        if (!$carrinho) {
            $carrinho = $this->createCarrinho();
        }

        $this->insertCarrinhoProduto($carrinho, $produto,1);

        $this->carrinhoRepository->save($carrinho);
    }

    /**
     * @return Carrinho
     */
    private function createCarrinho(): Carrinho
    {
        $carrinho = new Carrinho();
        if ($this->usuarioSession instanceof Usuario) {
            $carrinho->setUsuario($this->usuarioSession);
        }

        $hash = uniqid();
        $carrinho->setHash($hash);
        $carrinho->setStatus(StatusCarrinhoEnum::ABERTO);
        $carrinho->createdAt();

        if (!$this->usuarioSession instanceof Usuario) {
            $this->sessionService->createSession($this->sessionService::CARRINHO_SESSION, $hash);
        }

        return $carrinho;
    }

    private function insertCarrinhoProduto(
        Carrinho $carrinho,
        Produto $produto,
        int $quantidade = 1
    ) {
        $carrinhoProduto = $this->carrinhoProdutoRepository
            ->findOneBy(['carrinho' => $carrinho, 'produto' => $produto]);

        if (!$carrinhoProduto) {
            $carrinho->addProdutoCarrinho($this->createCarrinhoProduto($carrinho, $produto, $quantidade));

            return;
        }

        foreach ($carrinho->getProdutosCarrinho() as $carrinhoProduto) {
            if ($produto->getId() === $carrinhoProduto->getId()) {
                $carrinhoProduto->setQuantidade($carrinhoProduto->getQuantidade() + $quantidade);
            }
        };
    }

    /**
     * @param Carrinho $carrinho
     * @param Produto $produto
     * @param int $quantidade
     * @return CarrinhoProduto
     */
    private function createCarrinhoProduto(
        Carrinho $carrinho,
        Produto $produto,
        int $quantidade = 1
    ): CarrinhoProduto {
        $carrinhoProduto = new CarrinhoProduto();
        $carrinhoProduto->setCarrinho($carrinho);
        $carrinhoProduto->setProduto($produto);
        $carrinhoProduto->setQuantidade($quantidade);
        $carrinhoProduto->createdAt();

        return $carrinhoProduto;
    }

    /**
     * @param CarrinhoProduto $produto
     * @param int $quantidade
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateItem(CarrinhoProduto $produto, int $quantidade)
    {
        $this->isCarrinhoUsuarioLogado($produto->getCarrinho());
        $produto->setQuantidade($quantidade);
        $produto->updateAt();

        $this->carrinhoProdutoRepository->save($produto);
    }

    /**
     * @param Carrinho $carrinho
     */
    private function isCarrinhoUsuarioLogado(Carrinho $carrinho): void
    {
        if(
            $this->usuarioSession instanceof Usuario
            && !$carrinho->getUsuario() == $this->usuarioSession
        ) {
            throw new \DomainException('Carrinho não pertence a seu usuário');
        }
    }

    /**
     * @return object|null
     */
    public function getCarrinhoAberto()
    {
        if (!$this->usuarioSession instanceof Usuario) {
            return $this->carrinhoRepository->findOneBy(
                [
                    'hash' => $this->sessionService->getSession($this->sessionService::CARRINHO_SESSION),
                    'status' => StatusCarrinhoEnum::ABERTO
                ]
            );
        }

        return $this->carrinhoRepository
            ->findOneBy(
                [
                    'usuario' => $this->usuarioSession,
                    'status' => StatusCarrinhoEnum::ABERTO,
                ],
                ['id' => 'DESC']
            );
    }

    /**
     * @param Carrinho $carrinho
     * @return float
     */
    public function getValorTotalCarrinho(Carrinho $carrinho): float
    {
        $total = 0;

        foreach ($carrinho->getProdutosCarrinho() as $carrinhoProduto) {
            $valor = $carrinhoProduto->getProduto()->getValorVenda() * $carrinhoProduto->getQuantidade();

            $total += $valor;
        }

        return $total;
    }

    /**
     * @param Carrinho $carrinho
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function finalizar(Carrinho $carrinho): void
    {
        $this->isCarrinhoUsuarioLogado($carrinho);
        $this->vendaService->finalizarVenda($carrinho, $this->getValorTotalCarrinho($carrinho));
    }

    private function getCarrinhoSessao()
    {
        return $this->sessionService->getSession(SessionService::CARRINHO_SESSION);
    }

    /**
     * @param Carrinho $carrinho
     */
    private function updateCarrinhoSessao(Carrinho $carrinho)
    {
        if (!$this->usuarioSession) {
            $this->sessionService->updateSession(SessionService::CARRINHO_SESSION, $carrinho);
        }
    }

    public function atribuirCarrinho()
    {
        $hash = $this->getCarrinhoSessao();

        /** @var Carrinho $carrinho */
        $carrinho = $this->carrinhoRepository->findOneBy(['hash' => $hash, 'status' => StatusCarrinhoEnum::ABERTO]);

        if(!$carrinho) {
            throw new \DomainException('Não tem carrinho');
        }

        $carrinho->setUsuario($this->usuarioSession);
        $carrinho->updateAt();

        $this->carrinhoRepository->save($carrinho);
        $this->sessionService->removeSession($this->sessionService::CARRINHO_SESSION);
    }
}