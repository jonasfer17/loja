<?php

namespace App\Infrastructure\Service;

use App\Domain\Model\Produto\Colecao;
use App\Infrastructure\Doctrine\Repository\Produto\ColecaoRepository;
use App\Infrastructure\Enum\MensagemEnum;

class ColecaoService
{
    /**
     * @var ColecaoRepository
     */
    private $colecaoRepository;

    /**
     * ColecaoController constructor.
     * @param ColecaoRepository $colecaoRepository
     */
    public function __construct(
        ColecaoRepository $colecaoRepository
    ) {
        $this->colecaoRepository = $colecaoRepository;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAll(): array
    {
        return $this->colecaoRepository->findAll();
    }

    /**
     * @param int $id
     * @return Colecao
     */
    public function find(int $id): Colecao
    {
        $colecao = $this->colecaoRepository->findOneBy([
            'id' => $id
        ]);

        if (!$colecao) {
            throw new \DomainException(MensagemEnum::MENSAGEM_ERRO_NAO_AUTORIZADO);
        }

        return $colecao;
    }

    /**
     * @param Colecao $colecao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(Colecao $colecao): void
    {
        if ($this->hasColecaoMesmoNome($colecao)) {
            throw new \DomainException('Coleção com mesmo nome cadastrado!');
        }
        $colecao->createdAt();

        $this->colecaoRepository->salvar($colecao);
    }

    /**
     * @param Colecao $colecao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Colecao $colecao): void
    {
        if ($this->hasColecaoMesmoNome($colecao)) {
            throw new \DomainException('Coleção com mesmo nome cadastrada!');
        }
        $colecao->updateAt();

        $this->colecaoRepository->salvar($colecao);
    }

    /**
     * @param Colecao $colecao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Colecao $colecao)
    {
        $this->colecaoRepository->delete($colecao);
    }

    /**
     * @param Colecao $colecao
     * @return bool
     */
    private function hasColecaoMesmoNome(Colecao $colecao): bool
    {
        $colecaoBd = $this->colecaoRepository
            ->findOneBy(
                [
                    'nome' => $colecao->getNome()
                ]
            );

        return $colecaoBd && $colecaoBd->getId() != $colecao->getId();
    }
}