<?php

namespace App\Infrastructure\Service;

/**
 * Class HomeService
 * @package App\Infrastructure\Service
 */
class HomeService
{
    /**
     * @var ProdutoService
     */
    private $produtoService;

    /**
     * HomeService constructor.
     * @param ProdutoService $produtoService
     */
    public function __construct(ProdutoService $produtoService)
    {
        $this->produtoService = $produtoService;
    }

    /**
     * @return array
     */
    public function getProdutosNovos(): array
    {
        return $this->produtoService->getProdutosNovos();
    }

    /**
     * @return array
     */
    public function getProdutos(): array
    {
        return $this->produtoService->getProdutos();
    }
}