<?php

namespace App\Infrastructure\Service;

use App\Domain\Dto\MovimentacaoDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Empresa;
use App\Domain\Model\Financeiro\Movimentacao;
use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Usuario;
use App\Infrastructure\Assembler\Financeiro\MovimentacaoAssembler;
use App\Infrastructure\Doctrine\Repository\Financeiro\MovimentacaoRepository;

/**
 * Class MovimentacaoService
 * @package App\Infrastructure\Service
 */
class MovimentacaoService
{
    /**
     * @var MovimentacaoRepository
     */
    private $movimentacaoRepository;

    /**
     * @var MovimentacaoAssembler
     */
    private $movimentacaoAssembler;

    /**
     * MovimentacaoService constructor.
     * @param MovimentacaoRepository $movimentacaoRepository
     * @param MovimentacaoAssembler $movimentacaoAssembler
     */
    public function __construct(
        MovimentacaoRepository $movimentacaoRepository,
        MovimentacaoAssembler $movimentacaoAssembler
    ) {
        $this->movimentacaoRepository = $movimentacaoRepository;
        $this->movimentacaoAssembler = $movimentacaoAssembler;
    }

    public function create(
        MovimentacaoDto $movimentacaoDto,
        Produto $produto,
        Usuario $usuario,
        ?string $descricao
    ): Movimentacao {
        return $this->movimentacaoAssembler
            ->create(
                $movimentacaoDto,
                $produto,
                $usuario,
                $descricao
            );
    }

    /**
     * @param Movimentacao $movimentacao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Movimentacao $movimentacao): void
    {
        $this->movimentacaoRepository->salvar($movimentacao);
    }

    /**
     * @param Produto $produto
     * @return Movimentacao|null
     */
    public function findLastFromProduto(Produto $produto): ?Movimentacao
    {
        return $this->movimentacaoRepository->findOneBy(
            [
                'produto' => $produto
            ],
            ['id' => 'desc']
        );
    }
}