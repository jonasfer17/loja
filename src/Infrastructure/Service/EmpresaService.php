<?php

namespace App\Infrastructure\Service;

use App\Domain\Dto\EmpresaDto;
use App\Domain\Model\Ator;
use App\Domain\Model\Empresa;
use App\Domain\Model\Usuario;
use App\Infrastructure\Assembler\EmpresaAssembler;
use App\Infrastructure\Doctrine\Repository\AtorRepository;
use App\Infrastructure\Enum\MensagemEnum;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Throwable;

/**
 * Class EmpresaService
 */
class EmpresaService 
{
    
    /**
     * @var Usuario
     */
    private $usuario;

    /**
     * @var AtorRepository
     */
    private $atorRepository;

    /**
     * @var EmpresaAssembler
     */
    private $empresaAssembler;

    /**
     * @var PessoaService
     */
    private $pessoaService;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param AtorRepository $atorRepository
     * @param EmpresaAssembler $empresaAssembler
     * @param PessoaService $pessoaService
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AtorRepository $atorRepository,
        EmpresaAssembler $empresaAssembler,
        PessoaService $pessoaService
    ) {
        $this->usuario = $tokenStorage->getToken()->getUser();
        $this->atorRepository = $atorRepository;
        $this->empresaAssembler = $empresaAssembler;
        $this->pessoaService = $pessoaService;
    }

    /**
     * @return Ator
     */
    public function getEmpresa(): Ator
    {
        $ator = $this->atorRepository->findOneBy(['tipo' => 1]);
        
        if (!$ator) {
            throw new \DomainException(MensagemEnum::MENSAGEM_ERRO_NAO_AUTORIZADO);
        }

        return $ator;
    }

    /**
     * @return Usuario
     */
    public function getUsuario(): Usuario
    {
       return $this->usuario;
    }

    /**
     * @return EmpresaDto|null
     * @throws \Exception
     */
    public function getDadosEmpresaPrincipal(): ?EmpresaDto
    {
        $empresa = $this->atorRepository->getEmpresa($this->getEmpresa());

        return $this->empresaAssembler->writeDTO($empresa);
    }

    /**
     * @param EmpresaDto $empresaDto
     * 
     * @return void
     */
    public function save(EmpresaDto $empresaDto): void
    {
        try {
            $this->pessoaService->update($this->getEmpresa(), $empresaDto->getPessoa());
        } catch (Throwable $exception) {
            throw new Exception('Erro ao editar empresa');
        }
    }
}