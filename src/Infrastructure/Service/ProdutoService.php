<?php

namespace App\Infrastructure\Service;

use App\Domain\Dto\ProdutoDto;
use App\Domain\Model\Produto\Produto;
use App\Infrastructure\Assembler\Produto\ProdutoAssembler;
use App\Infrastructure\Doctrine\Repository\Produto\ProdutoRepository;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Validator\ProdutoValidator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class ProdutoService
 * @package App\Infrastructure\Service
 */
class ProdutoService
{
    /**
     * @var ProdutoRepository
     */
    private $produtoRepository;

    /**
     * @var ProdutoAssembler
     */
    private $produtoAssembler;

    /**
     * @var EmpresaService
     */
    private $empresaService;

    /**
     * @var MovimentacaoService
     */
    private $movimentacaoService;

    /**
     * @var ProdutoValidator
     */
    private $produtoValidator;

    /**
     * ProdutoService constructor.
     * @param ProdutoRepository $produtoRepository
     * @param ProdutoAssembler $produtoAssembler
     * @param EmpresaService $empresaService
     * @param MovimentacaoService $movimentacaoService
     * @param ProdutoValidator $produtoValidator
     */
    public function __construct(
        ProdutoRepository $produtoRepository,
        ProdutoAssembler $produtoAssembler,
        EmpresaService $empresaService,
        MovimentacaoService $movimentacaoService,
        ProdutoValidator $produtoValidator
    ) {
        $this->produtoRepository = $produtoRepository;
        $this->produtoAssembler = $produtoAssembler;
        $this->empresaService = $empresaService;
        $this->movimentacaoService = $movimentacaoService;
        $this->produtoValidator = $produtoValidator;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->produtoRepository->findAll();
    }

    /**
     * @param ProdutoDto $produtoDto
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(ProdutoDto $produtoDto)
    {
        $produto = $this->produtoAssembler->create($produtoDto);
        $movimentacao = $this->movimentacaoService->create(
            $produtoDto->getMovimentacao(),
            $produto,
            $this->empresaService->getUsuario(),
            'Cadastro de Produto'
        );

        $this->produtoRepository->salvar($produto);
        $this->movimentacaoService->salvar($movimentacao);
    }

    /**
     * @param ProdutoDto $produtoDto
     * @param Produto $produto
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function edit(ProdutoDto $produtoDto, Produto $produto): void
    {
        $this->produtoValidator->validate();

        $produto = $this->produtoAssembler->readDto($produtoDto, $produto);
        $movimentacao = $this->movimentacaoService->create(
            $produtoDto->getMovimentacao(),
            $produto,
            $this->empresaService->getUsuario(),
            'Edição de Produto'
        );

        $produto->updateAt();
        $this->produtoRepository->salvar($produto);
        $this->movimentacaoService->salvar($movimentacao);
    }

    /**
     * @param int $id
     * @return Produto|null
     */
    public function getDadosProduto(int $id): ?ProdutoDto
    {
        $produto = $this->produtoRepository->findOneBy([
            'id' => $id
        ]);

        if (!$produto) {
            throw new \DomainException(MensagemEnum::MENSAGEM_ERRO_NAO_AUTORIZADO);
        }

        $movimentacao = $this->movimentacaoService->findLastFromProduto($produto);

        return $this->produtoAssembler->writeDto($produto, $movimentacao);
    }

    public function delete(Produto $produto)
    {

    }

    /**
     * @return array
     */
    public function getProdutos(): array
    {
        return $this->produtoRepository->findAll();
    }

    /**
     * @return array
     */
    public function getProdutosNovos(): array
    {
        return $this->produtoRepository->findBy([], ['createdAt' => 'DESC'], 10);
    }

    /**
     * @return array
     */
    public function getProdutosRelacionados(): array
    {
        return $this->produtoRepository->findBy(['grupo' => 1]);
    }
}