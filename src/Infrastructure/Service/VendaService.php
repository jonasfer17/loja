<?php

namespace App\Infrastructure\Service;

use App\Domain\Dto\CheckoutDto;
use App\Domain\Model\Financeiro\Venda;
use App\Domain\Model\Loja\Carrinho;
use App\Infrastructure\Doctrine\Repository\Financeiro\VendaRepository;
use App\Infrastructure\Enum\StatusVendaEnum;
use App\Infrastructure\Service\Integracao\PagSeguroService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use GuzzleHttp\Client;

/**
 * Class VendaService
 * @package App\Infrastructure\Service
 */
class VendaService
{
    /**
     * @var VendaRepository
     */
    private $vendaRepository;

    /**
     * @var PagSeguroService
     */
    private $pagSeguroService;

    /**
     * VendaService constructor.
     * @param VendaRepository $vendaRepository
     * @param PagSeguroService $pagSeguroService
     */
    public function __construct(
        VendaRepository $vendaRepository,
        PagSeguroService $pagSeguroService
    ) {
        $this->vendaRepository = $vendaRepository;
        $this->pagSeguroService = $pagSeguroService;
    }

    /**
     * @param Venda $venda
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function salvar(Venda $venda)
    {
        $this->vendaRepository->save($venda);
    }

    /**
     * @param Carrinho $carrinho
     * @param float $valorTotal
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function finalizarVenda(Carrinho $carrinho, float $valorTotal): void
    {
        $venda = $this->vendaRepository->findOneBy(['carrinho' => $carrinho]);

        if (!$venda) {
            $this->criarVenda($carrinho, $valorTotal);
            return;
        }

        $venda->setValorTotal($valorTotal);
        $venda->updateAt();
        $this->vendaRepository->save($venda);
    }

    /**
     * @param Carrinho $carrinho
     * @return Venda|null
     */
    public function getVendaByCarrinho(Carrinho $carrinho): ?Venda
    {
        return $this->vendaRepository->findOneBy(['carrinho' => $carrinho]);
    }

    /**
     * @param Carrinho $carrinho
     * @param float $valorTotal
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function criarVenda(Carrinho $carrinho, float $valorTotal): void
    {
        $venda = new Venda();
        $venda->createdAt();
        $venda->setCarrinho($carrinho);
        $venda->setValorTotal($valorTotal);
        $venda->setStatus(StatusVendaEnum::ABERTA);

        $this->vendaRepository->save($venda);
    }

    /**
     * @param Venda $venda
     * @param CheckoutDto $checkoutDto
     * @return \SimpleXMLElement
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function checkout(Venda $venda, CheckoutDto $checkoutDto)
    {
        $response = $this->pagSeguroService->request($venda, $checkoutDto);
        $this->alterarStatusVenda($venda, StatusVendaEnum::AGUARDANDO_PAGAMENTO);

        return $response->code;
    }

    /**
     * @param Venda $venda
     * @param int $status
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function alterarStatusVenda(Venda $venda, int $status): void
    {
        $venda->setStatus($status);
        $venda->updateAt();

        $this->vendaRepository->save($venda);
    }
}