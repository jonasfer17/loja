<?php

namespace App\Infrastructure\Service\Integracao;

use App\Domain\Dto\CheckoutDto;
use App\Domain\Model\Financeiro\Venda;
use App\Domain\Model\Loja\CarrinhoProduto;
use App\Infrastructure\Utils\FloatConverter;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Class PagSeguroService
 * @package App\Infrastructure\Service\Integracao
 */
class PagSeguroService
{
    private const URL = 'https://ws.sandbox.pagseguro.uol.com.br/v2',
        TOKEN = '539DE17CB76C40A7A9AC31EBB0867B52',
        EMAIL = 'jonasferdev@gmail.com';

    public const URL_CHECKOUT = 'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client(['verify' => false ]);
    }

    /**
     * @param Venda $venda
     * @param CheckoutDto $checkoutDto
     * @return \SimpleXMLElement
     */
    public function request(Venda $venda, CheckoutDto $checkoutDto)
    {
        $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout?email=' . 'jonasferdev@gmail.com' . '&token=' . '539DE17CB76C40A7A9AC31EBB0867B52';
        $headers = ['Content-Type' => 'application/x-www-form-urlencoded'];
        $bodyDefault = [
            'token' => self::TOKEN,
            'email' => self::EMAIL,
            'currency' => 'BRL',
            'shippingAddressRequired' => 'false',
            'extraAmount' => '0.00',
            'reference' => '124665c23f7896adff508377925',
        ];


        $bodyCliente = [
            'senderName' => $checkoutDto->getPessoa()->getNome() . 'Da Silva',
            'senderAreaCode' => substr($checkoutDto->getPessoa()->getContato()->getTelefone(),0, 2),
            'senderPhone' => substr($checkoutDto->getPessoa()->getContato()->getTelefone(),2),
            'senderEmail' => 'c63864698272005710547@sandbox.pagseguro.com.br', // $venda->getCarrinho()->getUsuario()->getEmail();

        ];


        $bodyProdutos = $this->getProdutosVenda($venda);

        $body = array_merge($bodyDefault, $bodyCliente, $bodyProdutos);

        $response = $this->client->post(
            $url,
            [
                RequestOptions::HEADERS => $headers,
                'form_params' => $body
            ]
        );

        return simplexml_load_string($response->getBody()->getContents());
    }

    private function getProdutosVenda(Venda $venda)
    {
        $arrayProdutos = [];
        $index = 1;

        /** @var CarrinhoProduto $carrinho */
        foreach ($venda->getCarrinho()->getProdutosCarrinho() as $carrinho) {
            $arrayProdutos['itemId'.$index] = (string) $carrinho->getProduto()->getId();
            $arrayProdutos['itemDescription'.$index] = (string) $carrinho->getProduto()->getNome();
            $arrayProdutos['itemAmount'.$index] = (string) number_format($carrinho->getProduto()->getValorVenda(), 2);
            $arrayProdutos['itemQuantity'.$index] = (string) $carrinho->getQuantidade();

            $index++;
        }

        return $arrayProdutos;
    }
}