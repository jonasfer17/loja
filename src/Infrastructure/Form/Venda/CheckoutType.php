<?php

namespace App\Infrastructure\Form\Venda;

use App\Domain\Dto\CheckoutDto;
use App\Infrastructure\Form\ContatoType;
use App\Infrastructure\Form\EnderecoType;
use App\Infrastructure\Form\PessoaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CheckoutType
 * @package App\Infrastructure\Form\Venda
 */
class CheckoutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pessoa', PessoaType::class)
            ->add('formaPagamento', ChoiceType::class, [
                'choices' => [
                    'Trasferência' => 1,
                    'PagSeguro' => 2
                ],
                'multiple' => false,
                'expanded' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CheckoutDto::class,
            'attr' => [
                'class' => 'input-text'
            ]
        ]);
    }
}