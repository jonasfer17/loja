<?php

namespace App\Infrastructure\Form;

use App\Domain\Dto\EnderecoDto;
use App\Infrastructure\Enum\EstadosEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnderecoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rua', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('numero', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('cep', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('complemento', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('bairro', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('municipio', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('estado', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-control'
                ],
                'choices' => $this->getChoiceEstado()
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EnderecoDto::class,
        ]);
    }

    /**
     * @return array
     */
    private function getChoiceEstado(): array
    {
        $estados = ['Selecione o estado' => null];
        
        return array_merge($estados, EstadosEnum::getAllEstados());
    }
}
