<?php

namespace App\Infrastructure\Form\Animal;

use App\Domain\Model\Animal\Especie;
use App\Domain\Model\Animal\Raca;
use App\Infrastructure\Service\EmpresaService;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RacaType
 */
class RacaType extends AbstractType
{
    /**
     * @var EmpresaService
     */
    private $empresaService;

    /**
     * @param EmpresaService $empresaService
     * 
     * @return void
     */
    public function __construct(EmpresaService $empresaService)
    {
        $this->empresaService = $empresaService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => 'required'
            ])
            ->add('especie', EntityType::class, [
                'class' => Especie::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->where('e.ator = :empresa')
                        ->orderBy('e.nome', 'ASC')
                        ->setParameter('empresa', $this->empresaService->getEmpresa())
                    ;
                },
                'choice_label' => 'nome',
                'attr' => [
                    'class' => 'form-control show-tick'
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Raca::class,
        ]);
    }
}