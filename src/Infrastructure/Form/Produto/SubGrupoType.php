<?php

namespace App\Infrastructure\Form\Produto;

use App\Domain\Model\Produto\Grupo;
use App\Domain\Model\Produto\SubGrupo;
use App\Infrastructure\Enum\ConfiguracaoEnum;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubGrupoType
 * @package App\Infrastructure\Form\Produto
 */
class SubGrupoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nome', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true
            ])
            ->add('grupo', EntityType::class, [
                'class' => Grupo::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.nome', 'ASC')
                        ;
                },
                'choice_label' => 'nome',
                'attr' => [
                    'class' => 'form-control show-tick'
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * @return Void
     */
    public function configureOptions(OptionsResolver $resolver): Void
    {
        $resolver->setDefaults([
            'data_class' => SubGrupo::class,
        ]);
    }
}