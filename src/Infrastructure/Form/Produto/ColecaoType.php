<?php

namespace App\Infrastructure\Form\Produto;

use App\Domain\Model\Produto\Colecao;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ColecaoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nome', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * @return Void
     */
    public function configureOptions(OptionsResolver $resolver): Void
    {
        $resolver->setDefaults([
            'data_class' => Colecao::class,
        ]);
    }
}