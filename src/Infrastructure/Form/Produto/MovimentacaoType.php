<?php

namespace App\Infrastructure\Form\Produto;

use App\Domain\Dto\MovimentacaoDto;
use App\Infrastructure\Utils\FloatConverter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MovimentacaoType
 * @package App\Infrastructure\Form\Produto
 */
class MovimentacaoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantidade', TextType::class, [
                'label' => 'Estoque Atual',
                'attr' => ['class' => 'form-control decimal']
            ])
            ->add('valorCusto', TextType::class, [
                'label' => 'Preço de Custo (R$)',
                'attr' => ['class' => 'form-control decimal']
            ])
            ->add('valorVenda', TextType::class, [
                'label' => 'Preço de Venda (R$)',
                'attr' => [
                    'class' => 'form-control decimal',
                    'required' => true
                ]
            ])
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                [$this, 'onPreSubmit']
            );
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();

        $data['quantidade'] = FloatConverter::convert($data['quantidade']);
        $data['valorCusto'] = FloatConverter::convert($data['valorCusto']);
        $data['valorVenda'] = FloatConverter::convert($data['valorVenda']);

        $event->setData($data);
    }
    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MovimentacaoDto::class,
            'required' => false
        ]);
    }
}