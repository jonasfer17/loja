<?php

namespace App\Infrastructure\Form\Produto;

use App\Domain\Dto\GrupoProdutoDto;
use App\Domain\Model\Produto\Grupo;
use App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GrupoType
 * @package App\Infrastructure\Form\Produto
 */
class GrupoType extends AbstractType
{
    /**
     * @var GrupoRepository
     */
    private $grupoRepository;

    /**
     * GrupoType constructor.
     * @param GrupoRepository $grupoRepository
     */
    public function __construct(GrupoRepository $grupoRepository)
    {
        $this->grupoRepository = $grupoRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nome', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'required' => true
            ])
            ->add('grupoPai', ChoiceType::class, [
                'label' => 'Sub-grupo de',
                'choices' => $this->getGruposProduto(),
                'attr' => [
                    'class' => 'form-control show-tick'
                ],
            ])
        ;
    }

    /**
     * @param int $id
     * @return array
     */
    private function getGruposProduto(int $id = 0): array
    {
        $grupos = $this->grupoRepository->findAll();

        $arrayGrupo = ['Selecione' => 0];
        /** @var Grupo $grupo */
        foreach ($grupos as $grupo) {
            $arrayGrupo = array_merge($arrayGrupo, [$grupo->getNome() => $grupo->getId()]);
        }

        return $arrayGrupo;
    }

    /**
     * @param OptionsResolver $resolver
     * @return Void
     */
    public function configureOptions(OptionsResolver $resolver): Void
    {
        $resolver->setDefaults([
            'data_class' => GrupoProdutoDto::class,
        ]);
    }
}