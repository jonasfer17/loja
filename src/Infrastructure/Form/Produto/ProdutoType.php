<?php

namespace App\Infrastructure\Form\Produto;

use App\Domain\Dto\ProdutoDto;
use App\Domain\Model\Produto\Grupo;
use App\Domain\Model\Produto\Marca;
use App\Domain\Model\Produto\Raridade;
use App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository;
use App\Infrastructure\Enum\ConfiguracaoEnum;
use App\Infrastructure\Service\EmpresaService;
use App\Infrastructure\Service\MarcaService;
use App\Infrastructure\Service\GrupoProdutoService;
use App\Infrastructure\Utils\FloatConverter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProdutoType
 * @package App\Infrastructure\Form\Produto
 */
class ProdutoType extends AbstractType
{
    /**
     * @var EmpresaService
     */
    private $empresaService;

    /**
     * @var GrupoRepository
     */
    private $grupoRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * ProdutoType constructor.
     * @param EmpresaService $empresaService
     * @param GrupoRepository $grupoRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EmpresaService $empresaService,
        GrupoRepository $grupoRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->empresaService = $empresaService;
        $this->grupoRepository = $grupoRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                ],
            ])
            ->add('grupo', ChoiceType::class, [
                'choices' => $this->getGruposProduto(),
                'attr' => [
                    'class' => 'form-control show-tick'
                ],
            ])
            ->add('descricao', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => false
            ])
            ->add('idioma', ChoiceType::class, [
                'choices' => $this->getIdiomas(),
                'attr' => [
                    'class' => 'form-control show-tick',
                ]
            ])
            ->add('raridade', ChoiceType::class, [
                'choices' => $this->getRaridades(),
                'attr' => [
                    'class' => 'form-control show-tick',
                ]
            ])
            ->add('colecao', ChoiceType::class, [
                'choices' => $this->getRaridades(),
                'attr' => [
                    'class' => 'form-control show-tick',
                ]
            ])
            ->add('movimentacao', MovimentacaoType::class)
        ;
    }

    /**
     * @return array
     */
    private function getRaridades(): array
    {
        $arrayRaridades = ['Selecione' => 0];
        $raridades = $this->entityManager->getRepository(Raridade::class)->findAll();

        foreach ($raridades as $raridade) {
            $arrayRaridades = array_merge($arrayRaridades, [$raridade->getNome() => $raridade->getId()]);
        }

        return $arrayRaridades;
    }

    /**
     * @return array
     */
    private function getIdiomas(): array
    {
        return [
            'Selecione' => null,
            'Português' => 'PT',
            'Japonês' => 'JP',
            'Inglês' => 'IN'
        ];
    }

    /**
     * @return array
     */
    private function getGruposProduto(): array
    {
        $grupos = $this->grupoRepository->findAll();

        $gruposArray = ['Selecione' => 0];

        /** @var Grupo $grupo */
        foreach ($grupos as $grupo) {
            if(!$grupo->getGrupoPai()) {
                $gruposArray[$grupo->getNome()] = $this->isPai($grupo->getId()) ? [] : $grupo->getId();
                continue;
            }

            if (!isset($gruposArray[$grupo->getGrupoPai()->getNome()])) {
                $gruposArray[$grupo->getGrupoPai()->getNome()] = [];
            }

            $gruposArray[$grupo->getGrupoPai()->getNome()] =
                array_merge($gruposArray[$grupo->getGrupoPai()->getNome()], [$grupo->getNome() => $grupo->getId()]);

        }

        return $gruposArray;
    }

    /**
     * @param int $id
     * @return bool
     */
    private function isPai(int $id): bool
    {
        return (bool) $this->grupoRepository->findOneBy(['grupoPai' => $id]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProdutoDto::class,
            'required' => false
        ]);
    }
}