<?php

namespace App\Infrastructure\Form;

use App\Domain\Dto\PessoaDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PessoaType
 */
class PessoaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tipo', ChoiceType::class, [
                'choices'  => [
                    'Pessoa Física' => 1,
                    'Pessoa Jurídica' => 2,
                ],
                'attr' => [
                    'class' => 'form-control show-tick'
                ]
            ])
            ->add('cpfCnpj', TextType::class, [
                'label' => 'CPF',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('nome', TextType::class, [
                'label' => 'Nome Completo',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('contato', ContatoType::class)
            ->add('endereco', EnderecoType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PessoaDto::class,
        ]);
    }
}
