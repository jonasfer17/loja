<?php

namespace App\Infrastructure\Form\Usuario;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => 'Email*',
                'required' => true
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Senha*',
                'required' => true
            ])
        ;
    }
}