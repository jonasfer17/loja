<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200519230157 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produto_sub_grupo ADD id_grupo INT DEFAULT NULL');
        $this->addSql('ALTER TABLE produto_sub_grupo ADD CONSTRAINT FK_FBBD0E1B628BDAE3 FOREIGN KEY (id_grupo) REFERENCES produto_grupo (id)');
        $this->addSql('CREATE INDEX IDX_FBBD0E1B628BDAE3 ON produto_sub_grupo (id_grupo)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produto_sub_grupo DROP FOREIGN KEY FK_FBBD0E1B628BDAE3');
        $this->addSql('DROP INDEX IDX_FBBD0E1B628BDAE3 ON produto_sub_grupo');
        $this->addSql('ALTER TABLE produto_sub_grupo DROP id_grupo');
    }
}
