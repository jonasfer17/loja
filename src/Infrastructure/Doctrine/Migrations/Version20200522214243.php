<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200522214243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE carrinho_produto (id INT AUTO_INCREMENT NOT NULL, id_carrinho INT NOT NULL, id_produto INT NOT NULL, quantidade INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_6D2BC499F7EA0F55 (id_carrinho), UNIQUE INDEX UNIQ_6D2BC4998231E0A7 (id_produto), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE carrinho_produto ADD CONSTRAINT FK_6D2BC499F7EA0F55 FOREIGN KEY (id_carrinho) REFERENCES carrinho (id)');
        $this->addSql('ALTER TABLE carrinho_produto ADD CONSTRAINT FK_6D2BC4998231E0A7 FOREIGN KEY (id_produto) REFERENCES produto (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE carrinho_produto');
    }
}
