<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200519222345 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ator (id INT AUTO_INCREMENT NOT NULL, tipo INT NOT NULL, nome VARCHAR(255) NOT NULL, cpf_cnpj VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contato (id INT AUTO_INCREMENT NOT NULL, id_ator INT NOT NULL, email VARCHAR(255) DEFAULT NULL, telefone VARCHAR(255) DEFAULT NULL, celular VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_C384AB422668F48C (id_ator), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empresa (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE endereco (id INT AUTO_INCREMENT NOT NULL, id_ator INT NOT NULL, rua VARCHAR(255) DEFAULT NULL, numero INT DEFAULT NULL, cep VARCHAR(255) DEFAULT NULL, bairro VARCHAR(255) DEFAULT NULL, municipio VARCHAR(255) DEFAULT NULL, estado VARCHAR(255) DEFAULT NULL, pais VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_F8E0D60E2668F48C (id_ator), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movimentacao (id INT AUTO_INCREMENT NOT NULL, id_produto INT DEFAULT NULL, id_usuario INT NOT NULL, quantidade NUMERIC(10, 2) NOT NULL, ajuste NUMERIC(10, 2) NOT NULL, descricao VARCHAR(255) DEFAULT NULL, valor_venda NUMERIC(6, 2) NOT NULL, valor_compra NUMERIC(6, 2) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_C1BF366A8231E0A7 (id_produto), INDEX IDX_C1BF366AFCF8192D (id_usuario), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produto_colecao (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produto_condicao (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produto_grupo (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(255) NOT NULL, ativo TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produto (id INT AUTO_INCREMENT NOT NULL, id_grupo INT DEFAULT NULL, id_raridade INT DEFAULT NULL, id_colecao INT DEFAULT NULL, nome VARCHAR(255) NOT NULL, idioma VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_5CAC49D7628BDAE3 (id_grupo), INDEX IDX_5CAC49D7E59DA181 (id_raridade), INDEX IDX_5CAC49D7BB4D53D2 (id_colecao), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produto_raridade (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produto_tipo (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, id_ator INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_2265B05DE7927C74 (email), UNIQUE INDEX UNIQ_2265B05D2668F48C (id_ator), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contato ADD CONSTRAINT FK_C384AB422668F48C FOREIGN KEY (id_ator) REFERENCES ator (id)');
        $this->addSql('ALTER TABLE endereco ADD CONSTRAINT FK_F8E0D60E2668F48C FOREIGN KEY (id_ator) REFERENCES ator (id)');
        $this->addSql('ALTER TABLE movimentacao ADD CONSTRAINT FK_C1BF366A8231E0A7 FOREIGN KEY (id_produto) REFERENCES produto (id)');
        $this->addSql('ALTER TABLE movimentacao ADD CONSTRAINT FK_C1BF366AFCF8192D FOREIGN KEY (id_usuario) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE produto ADD CONSTRAINT FK_5CAC49D7628BDAE3 FOREIGN KEY (id_grupo) REFERENCES produto_grupo (id)');
        $this->addSql('ALTER TABLE produto ADD CONSTRAINT FK_5CAC49D7E59DA181 FOREIGN KEY (id_raridade) REFERENCES produto_raridade (id)');
        $this->addSql('ALTER TABLE produto ADD CONSTRAINT FK_5CAC49D7BB4D53D2 FOREIGN KEY (id_colecao) REFERENCES produto_colecao (id)');
        $this->addSql('ALTER TABLE usuario ADD CONSTRAINT FK_2265B05D2668F48C FOREIGN KEY (id_ator) REFERENCES ator (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contato DROP FOREIGN KEY FK_C384AB422668F48C');
        $this->addSql('ALTER TABLE endereco DROP FOREIGN KEY FK_F8E0D60E2668F48C');
        $this->addSql('ALTER TABLE usuario DROP FOREIGN KEY FK_2265B05D2668F48C');
        $this->addSql('ALTER TABLE produto DROP FOREIGN KEY FK_5CAC49D7BB4D53D2');
        $this->addSql('ALTER TABLE produto DROP FOREIGN KEY FK_5CAC49D7628BDAE3');
        $this->addSql('ALTER TABLE movimentacao DROP FOREIGN KEY FK_C1BF366A8231E0A7');
        $this->addSql('ALTER TABLE produto DROP FOREIGN KEY FK_5CAC49D7E59DA181');
        $this->addSql('ALTER TABLE movimentacao DROP FOREIGN KEY FK_C1BF366AFCF8192D');
        $this->addSql('DROP TABLE ator');
        $this->addSql('DROP TABLE contato');
        $this->addSql('DROP TABLE empresa');
        $this->addSql('DROP TABLE endereco');
        $this->addSql('DROP TABLE movimentacao');
        $this->addSql('DROP TABLE produto_colecao');
        $this->addSql('DROP TABLE produto_condicao');
        $this->addSql('DROP TABLE produto_grupo');
        $this->addSql('DROP TABLE produto');
        $this->addSql('DROP TABLE produto_raridade');
        $this->addSql('DROP TABLE produto_tipo');
        $this->addSql('DROP TABLE usuario');
    }
}
