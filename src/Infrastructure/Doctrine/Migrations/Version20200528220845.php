<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200528220845 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE produto_sub_grupo');
        $this->addSql('ALTER TABLE produto_grupo ADD id_grupo_pai INT DEFAULT NULL');
        $this->addSql('ALTER TABLE produto_grupo ADD CONSTRAINT FK_3EF51089914C3466 FOREIGN KEY (id_grupo_pai) REFERENCES produto_grupo (id)');
        $this->addSql('CREATE INDEX IDX_3EF51089914C3466 ON produto_grupo (id_grupo_pai)');
        $this->addSql('ALTER TABLE produto ADD descricao VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE produto_sub_grupo (id INT AUTO_INCREMENT NOT NULL, id_grupo INT DEFAULT NULL, nome VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_FBBD0E1B628BDAE3 (id_grupo), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE produto_sub_grupo ADD CONSTRAINT FK_FBBD0E1B628BDAE3 FOREIGN KEY (id_grupo) REFERENCES produto_grupo (id)');
        $this->addSql('ALTER TABLE produto DROP descricao');
        $this->addSql('ALTER TABLE produto_grupo DROP FOREIGN KEY FK_3EF51089914C3466');
        $this->addSql('DROP INDEX IDX_3EF51089914C3466 ON produto_grupo');
        $this->addSql('ALTER TABLE produto_grupo DROP id_grupo_pai');
    }
}
