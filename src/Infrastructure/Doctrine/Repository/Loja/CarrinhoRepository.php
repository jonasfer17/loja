<?php

namespace App\Infrastructure\Doctrine\Repository\Loja;

use App\Domain\Model\Loja\Carrinho;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class CarrinhoRepository
 * @package App\Infrastructure\Doctrine\Repository\Loja
 */
class CarrinhoRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Carrinho::class);
    }

    /**
     * @param Carrinho $carrinho
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Carrinho $carrinho): void
    {
        $this->getEntityManager()->persist($carrinho);
        $this->getEntityManager()->flush();
    }
}