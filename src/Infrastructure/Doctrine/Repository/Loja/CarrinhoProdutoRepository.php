<?php

namespace App\Infrastructure\Doctrine\Repository\Loja;

use App\Domain\Model\Loja\CarrinhoProduto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class CarrinhoProdutoRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CarrinhoProduto::class);
    }

    /**
     * @param CarrinhoProduto $carrinhoProduto
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(CarrinhoProduto $carrinhoProduto): void
    {
        $this->getEntityManager()->persist($carrinhoProduto);
        $this->getEntityManager()->flush();
    }
}