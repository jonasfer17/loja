<?php

namespace App\Infrastructure\Doctrine\Repository\Produto;

use App\Domain\Model\Produto\Grupo;
use App\Domain\Model\Produto\SubGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class SubGrupoRepository
 * @package App\Infrastructure\Doctrine\Repository\Produto
 */
class SubGrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubGrupo::class);
    }

    /**
     * @param SubGrupo $subGrupo
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(SubGrupo $subGrupo): void
    {
        $this->getEntityManager()->persist($subGrupo);
        $this->getEntityManager()->flush();
    }

    /**
     * @param SubGrupo $subGrupo
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(SubGrupo $subGrupo): void
    {
        $this->getEntityManager()->remove($subGrupo);
        $this->getEntityManager()->flush();
    }
}