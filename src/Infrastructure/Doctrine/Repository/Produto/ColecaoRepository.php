<?php


namespace App\Infrastructure\Doctrine\Repository\Produto;

use App\Domain\Model\Produto\Colecao;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ColecaoRepository
 * @package App\Infrastructure\Doctrine\Repository\Produto
 */
class ColecaoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Colecao::class);
    }

    /**
     * @param Colecao $colecao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Colecao $colecao): void
    {
        $this->getEntityManager()->persist($colecao);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Colecao $colecao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Colecao $colecao): void
    {
        $this->getEntityManager()->remove($colecao);
        $this->getEntityManager()->flush();
    }
}