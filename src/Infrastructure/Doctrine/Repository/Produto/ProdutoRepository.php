<?php

namespace App\Infrastructure\Doctrine\Repository\Produto;

use App\Domain\Model\Ator;
use App\Domain\Model\Empresa;
use App\Domain\Model\Financeiro\Movimentacao;
use App\Domain\Model\Produto\Produto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ProdutoRepository
 * @package App\Infrastructure\Doctrine\Repository\Produto
 */
class ProdutoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produto::class);
    }

    /**
     * @param Produto $produto
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Produto $produto): void
    {
        $this->getEntityManager()->persist($produto);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Produto $produto
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Produto $produto): void
    {
        $this->getEntityManager()->remove($produto);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Empresa $empresa
     * @return array
     */
    public function getAllProdutos(Empresa $empresa): array
    {
        return $this->createQueryBuilder('p')
            ->select(
                'p.id',
                'p.nome'
            )
            ->where('p.empresa = :empresa')
            ->setParameter('empresa', $empresa)
            ->getQuery()
            ->getResult();
    }
}