<?php

namespace App\Infrastructure\Doctrine\Repository\Produto;

use App\Domain\Model\Produto\Marca;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class MarcaRepository
 * @package App\Infrastructure\Repository
 */
class MarcaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Marca::class);
    }

    /**
     * @param Marca $marca
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Marca $marca): void
    {
        $this->getEntityManager()->persist($marca);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Marca $marca
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Marca $marca): void
    {
        $this->getEntityManager()->remove($marca);
        $this->getEntityManager()->flush();
    }
}