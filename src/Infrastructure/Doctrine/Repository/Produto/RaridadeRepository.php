<?php

namespace App\Infrastructure\Doctrine\Repository\Produto;

use App\Domain\Model\Produto\Raridade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class RaridadeRepository
 * @package App\Infrastructure\Doctrine\Repository\Produto
 */
class RaridadeRepository extends ServiceEntityRepository
{
    /**
     * RaridadeRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Raridade::class);
    }

    /**
     * @param Raridade $raridade
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function salvar(Raridade $raridade): void
    {
        $this->getEntityManager()->persist($raridade);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Raridade $raridade
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Raridade $raridade): void
    {
        $this->getEntityManager()->remove($raridade);
        $this->getEntityManager()->flush();
    }
}