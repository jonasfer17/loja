<?php

namespace App\Infrastructure\Doctrine\Repository\Produto;

use App\Domain\Model\Produto\Grupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class MarcaRepository
 * @package App\Infrastructure\Repository
 */
class GrupoRepository extends ServiceEntityRepository
{
    /**
     * GrupoRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Grupo::class);
    }

    /**
     * @param Grupo $grupo
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function salvar(Grupo $grupo): void
    {
        $this->getEntityManager()->persist($grupo);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Grupo $grupo
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Grupo $grupo): void
    {
        $this->getEntityManager()->remove($grupo);
        $this->getEntityManager()->flush();
    }
}