<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Model\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class UsuarioRepository
 */
class UsuarioRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * 
     * @return void
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Usuario::class);
    }
}
