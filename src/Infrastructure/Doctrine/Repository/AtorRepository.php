<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Model\Ator;
use App\Domain\Model\Contato;
use App\Domain\Model\Endereco;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * Class PessoaRepository
 */
class AtorRepository extends ServiceEntityRepository
{

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ator::class);
    }

    /**
     * @param Ator $ator
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Ator $ator): void
    {
        $this->getEntityManager()->persist($ator);
        $this->getEntityManager()->flush($ator);
    }

    /**
     * @param Ator $empresa
     * @return array
     * @throws Exception
     */
    public function getEmpresa(Ator $empresa): array
    {
        $empresa = $this->createQueryBuilder('e')
            ->select(
                'p.nome',
                'p.tipo',
                'p.cpfCnpj',
                'ed.rua',
                'ed.numero',
                'ed.cep',
                'ed.bairro',
                'ed.municipio',
                'ed.estado',
                'c.email',
                'c.telefone',
                'c.celular'
            )
            ->join(Ator::class, 'p', 'WITH', 'e.id = p.empresa')
            ->leftJoin(Endereco::class, 'ed', 'WITH', 'ed.ator = p.id')
            ->leftJoin(Contato::class, 'c', 'WITH', 'c.ator = p.id')
            ->where('e.id = :empresa')
            ->setParameter('empresa', $empresa)
            ->getQuery()
            ->getResult()
        ;

        if (empty($empresa)) {
            throw new Exception('Nenhuma empresa encontrado');
        }

        return current($empresa);
    }
}
