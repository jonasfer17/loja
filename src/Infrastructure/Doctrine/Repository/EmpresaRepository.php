<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Model\Ator;
use App\Domain\Model\Contato;
use App\Domain\Model\Empresa;
use App\Domain\Model\Endereco;
use App\Domain\Model\Pessoa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * Class EmpresaRepository
 */
class EmpresaRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * 
     * @return void
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Empresa::class);
    }

    public function getEmpresa(Empresa $empresa): array
    {
        $empresa = $this->createQueryBuilder('e')
            ->select(
                'p.nome',
                'p.tipo',
                'p.cpfCnpj',
                'ed.rua',
                'ed.numero',
                'ed.cep',
                'ed.bairro',
                'ed.municipio',
                'ed.estado',
                'c.email',
                'c.telefone',
                'c.celular'
            )
            ->join(Ator::class, 'p', 'WITH', 'e.id = p.empresa')
            ->leftJoin(Endereco::class, 'ed', 'WITH', 'ed.ator = p.id')
            ->leftJoin(Contato::class, 'c', 'WITH', 'c.ator = p.id')
            ->where('e.id = :empresa')
            ->setParameter('empresa', $empresa)
            ->getQuery()
            ->getResult()
        ;

        if (empty($empresa)) {
            throw new Exception('Nenhuma empresa encontrado');
        }

        return current($empresa);
    }

    /**
     * @param Empresa $empresa
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Empresa $empresa): void
    {
        $this->getEntityManager()->persist($empresa);
        $this->getEntityManager()->flush($empresa);
    }
}
