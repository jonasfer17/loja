<?php

namespace App\Infrastructure\Doctrine\Repository\Financeiro;

use App\Domain\Model\Financeiro\Venda;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class VendaRepository
 * @package App\Infrastructure\Doctrine\Repository\Financeiro
 */
class VendaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Venda::class);
    }

    /**
     * @param Venda $venda
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Venda $venda)
    {
        $this->getEntityManager()->persist($venda);
        $this->getEntityManager()->flush();
    }
}