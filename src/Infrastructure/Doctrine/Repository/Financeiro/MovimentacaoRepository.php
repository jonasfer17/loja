<?php


namespace App\Infrastructure\Doctrine\Repository\Financeiro;

use App\Domain\Model\Financeiro\Movimentacao;
use App\Domain\Model\Produto\Produto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * Class MovimentacaoRepository
 * @package App\Infrastructure\Doctrine\Repository\Financeiro
 */
class MovimentacaoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movimentacao::class);
    }

    /**
     * @param Movimentacao $movimentacao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Movimentacao $movimentacao): void
    {
        $this->getEntityManager()->persist($movimentacao);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Movimentacao $movimentacao
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Movimentacao $movimentacao): void
    {
        $this->getEntityManager()->remove($movimentacao);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Produto $produto
     * @return int|null
     */
    public function getLastAjuste(Produto $produto): ?int
    {
        $queryBuilder =  $this->getEntityManager()
            ->createQueryBuilder();

        return $queryBuilder->select('m.ajuste')
            ->from('App:Financeiro\Movimentacao', 'm')
            ->where('m.produto = :produto')
            ->orderBy('m.id ASC')
            ->setParameter('produto', $produto)
            ->getQuery()
            ->getFirstResult();
    }

    /**
     * @param array $produtos
     * @return int|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalProdutos(array $produtos)
    {
        return $this->createQueryBuilder()
            ->select('SUM(m.valor_venda)')
            ->from('App:Financeiro\Movimentacao', 'm')
            ->where('m.produto IN (:produtos)')
            ->setParameter('produtos', $produtos)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Produto $produto
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUltimaMovimentacao(Produto $produto): ?Movimentacao
    {
        return $this->createQueryBuilder('m')
            ->where('m.produto = :produto')
            ->orderBy('m.id', 'ASC')
            ->setParameter('produto', $produto)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param Produto $produto
     */
    public function UpdateAllMovimentacoesProduto(Produto $produto): void
    {
        $queryBuilder =  $this->getEntityManager()
            ->createQueryBuilder();

        $queryBuilder->update()
            ->from('App:Financeiro\Movimentacao', 'm')
            ->set('m.produto', 'null')
            ->where('m.produto = :produto')
            ->setParameter('produto', $produto)
            ->getQuery()
            ->execute();
    }
}