<?php

namespace App\Infrastructure\Validator;

use App\Domain\Dto\GrupoProdutoDto;
use App\Domain\Model\Produto\Grupo;
use App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository;
use App\Infrastructure\Enum\MensagemEnum;

class GrupoProdutoValidator
{
    /**
     * @var GrupoRepository
     */
    private $grupoRepository;

    /**
     * GrupoProdutoValidator constructor.
     * @param GrupoRepository $grupoRepository
     */
    public function __construct(GrupoRepository $grupoRepository)
    {
        $this->grupoRepository = $grupoRepository;
    }

    /**
     * @param GrupoProdutoDto $grupoProdutoDto
     * @param int $id
     */
    public function validate(GrupoProdutoDto $grupoProdutoDto, Grupo $grupo = null): void
    {
        $this->hasGrupoMesmoNome($grupoProdutoDto, $grupo ? $grupo->getId() : 0);
        $this->isMesmoGrupoDoPai($grupoProdutoDto, $grupo);
        $this->isSubGrupo($grupoProdutoDto, $grupo);
    }

    /**
     * @param GrupoProdutoDto $grupo
     */
    private function hasGrupoMesmoNome(GrupoProdutoDto $grupo, int $id): void
    {
        $grupoBd = $this->grupoRepository
            ->findOneBy(
                [
                    'nome' => $grupo->getNome()
                ]
            );

        if($grupoBd && $grupoBd->getId() != $id) {
            throw new \DomainException('Grupo com mesmo nome cadastrado!');
        }
    }

    /**
     * @param GrupoProdutoDto $grupoProdutoDto
     * @param Grupo|null $grupo
     */
    private function isMesmoGrupoDoPai(GrupoProdutoDto $grupoProdutoDto, Grupo $grupo = null)
    {
        if (
            $grupo
            && $grupoProdutoDto->getGrupoPai() === $grupo->getId()) {
            throw new \DomainException('Grupo não pode ser sub-grupo dele mesmo!');
        }
    }

    /**
     * @param Grupo $grupo
     */
    private function isGrupoMesmaEmpresa(Grupo $grupo) {
        $grupo = $this->grupoRepository->findOneBy([
            'id' => $grupo->getId()
        ]);

        if (!$grupo) {
            throw new \DomainException(MensagemEnum::MENSAGEM_ERRO_NAO_AUTORIZADO);
        }
    }
}