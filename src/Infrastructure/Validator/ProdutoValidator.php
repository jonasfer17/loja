<?php

namespace App\Infrastructure\Validator;

use App\Infrastructure\Doctrine\Repository\Produto\ProdutoRepository;

/**
 * Class ProdutoValidator
 * @package App\Infrastructure\Validator
 *
 */
class ProdutoValidator
{
    /**
     * @var ProdutoRepository
     */
    private $produtoRepository;

    /**
     * ProdutoValidator constructor.
     * @param ProdutoRepository $produtoRepository
     */
    public function __construct(ProdutoRepository $produtoRepository)
    {
        $this->produtoRepository = $produtoRepository;
    }

    public function validate(): void
    {
    }
}