<?php

namespace App\Infrastructure\TwigExtension;

use App\Infrastructure\Utils\MenuItens;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class MenuExtension
 */
class MenuExtension extends AbstractExtension
{
    /**
     * @var array
     */
    private $menu;

    /**
     * @var MenuItens
     */
    private $menuItens;

    /**
     * @param MenuItens $menuItens
     */
    public function __construct(MenuItens $menuItens)
    {
        $this->menu = [];
        $this->menuItens = new MenuItens();
    }
    
    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('createMenu', [$this, 'createMenu']),
        ];
    }

    /**
     * @return array
     */
    public function createMenu(): array
    {  
        $this->menu[] = $this->menuItens->createMenuDashBoard();
        $this->menu[] = $this->menuItens->createMenuProduto();
        $this->menu[] = $this->menuItens->createMenuConfiguracao();

        return $this->menu;
    }

    
}