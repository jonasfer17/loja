<?php


namespace App\Infrastructure\TwigExtension;

use App\Domain\Model\Financeiro\Movimentacao;
use App\Domain\Model\Produto\Produto;
use App\Infrastructure\Doctrine\Repository\Financeiro\MovimentacaoRepository;
use App\Infrastructure\Doctrine\Repository\Produto\ProdutoRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ProdutoExtension
 * @package App\Infrastructure\TwigExtension
 */
class ProdutoExtension extends AbstractExtension
{
    /**
     * @var MovimentacaoRepository
     */
    private $movimentacaoRepository;

    public function __construct(MovimentacaoRepository $movimentacaoRepository)
    {
        $this->movimentacaoRepository = $movimentacaoRepository;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getUltimaMovimentacao', [$this, 'getUltimaMovimentacao']),
        ];
    }

    /**
     * @param Produto $produto
     * @return Movimentacao|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUltimaMovimentacao(Produto $produto): ?Movimentacao
    {
        return $this->movimentacaoRepository->getUltimaMovimentacao($produto);
    }
}