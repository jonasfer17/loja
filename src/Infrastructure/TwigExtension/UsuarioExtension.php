<?php

namespace App\Infrastructure\TwigExtension;

use App\Domain\Dto\UsuarioDto;
use App\Domain\Model\Usuario;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface as TokenStorageTokenStorageInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class UsuarioExtension
 */
class UsuarioExtension extends AbstractExtension
{
    /**
     * @var Usuario|null
     */
    private $usuario;

    
    /**
     * @var UsuarioDto
     */
    private $usuarioDto;

    /**
     * @param TokenStorageInterface $tokenStorageInterface
     * @param UsuarioDto $usuarioDto
     */
    public function __construct(
        TokenStorageInterface $tokenStorageInterface,
        UsuarioDto $usuarioDto
    ) {
        $this->usuario = $tokenStorageInterface->getToken()
            ? $tokenStorageInterface->getToken()->getUser()
            : null;
        $this->usuarioDto = $usuarioDto;
        $this->convertUsuario();
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getUsuario', [$this, 'getUsuario']),
        ];
    }

    /**
     * @return UsuarioDto
     */
    public function getUsuario(): UsuarioDto
    {
        return $this->usuarioDto;
    }

    private function convertUsuario(): void
    {
        if ($this->usuario instanceof Usuario) {
            $this->usuarioDto->setId($this->usuario->getId());
            $this->usuarioDto->setNome($this->usuario->getAtor()->getNome());
            $this->usuarioDto->setEmail($this->usuario->getEmail());
            $this->usuarioDto->setRoles($this->usuario->getRoles());
        }
    }
}