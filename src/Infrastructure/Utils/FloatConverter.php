<?php

namespace App\Infrastructure\Utils;

/**
 * Class FloatConverter
 * @package App\Infrastructure\Utils
 */
class FloatConverter
{
    /**
     * @param string|null $valor
     * @return string|null
     */
    public static function convert(?string $valor): ?string
    {
        return $valor
            ? str_replace(',', '.', str_replace('.', '', $valor))
            : null;
    }

    /**
     * @param float|null $valor
     * @return string|null
     */
    public static function revert(?float $valor): ?string
    {
        return $valor
            ? number_format($valor, 2, ',', '.')
            : null;
    }
}