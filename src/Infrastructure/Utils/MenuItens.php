<?php
namespace App\Infrastructure\Utils;

use App\Domain\Dto\MenuDto;

/**
 * Class MenuItens
 */
class MenuItens
{
    /**
     * @return MenuDto
     */
    public function createMenuDashBoard(): MenuDto
    {
        return new MenuDto(
            'DashBoard',
            'DashBoard',
            'admin_dashboard',
            null,
            'home'
        );
    }
    /**
     * @return MenuDto
     */
    public function createMenuProduto(): MenuDto
    {
        $submenu = [];
        $submenu[] = $this->createSubMenuProduto();
        $submenu[] = $this->createSubMenuGrupoProduto();
        $submenu[] = $this->createSubMenuColecao();
        $submenu[] = $this->createSubMenuRaridade();

        return new MenuDto(
            'Produto',
            'Produto',
            null,
            $submenu,
            'event_note'
        );
    }

    public function createMenuConfiguracao(): MenuDto
    {
        $submenu = [];
        $submenu[] = $this->createSubMenuEmpresa();

        return new MenuDto(
            'Configuração',
            'Configuracao',
            null,
            $submenu,
            'settings'
        );
    }

    /**
     * @return MenuDto
     */
    private function createSubMenuGrupoProduto(): MenuDto
    {
        return new MenuDto(
            'Grupo',
            'Grupo',
            'admin_produto_grupo',
            null,
            null
        );
    }

    /**
     * @return MenuDto
     */
    private function createSubMenuColecao(): MenuDto
    {
        return new MenuDto(
            'Coleções',
            'Colecao',
            'admin_produto_colecao',
            null,
            null
        );
    }

    /**
     * @return MenuDto
     */
    private function createSubMenuRaridade(): MenuDto
    {
        return new MenuDto(
            'Raridades',
            'Raridade',
            'admin_produto_raridade',
            null,
            null
        );
    }

    /**
     * @return MenuDto
     */
    private function createSubMenuProduto(): MenuDto
    {
        return new MenuDto(
            'Produtos',
            'Produto',
            'admin_produto_item',
            null,
            null
        );
    }


    private function createSubMenuEmpresa(): MenuDto
    {
        return new MenuDto(
            'Empresa',
            'Empresa',
            'admin_configuracao_empresa',
            null,
            null
        );
    }
}