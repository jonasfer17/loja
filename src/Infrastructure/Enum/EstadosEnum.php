<?php

namespace App\Infrastructure\Enum;

class EstadosEnum
{
    public const 
        ACRE                = 'AC',
        ALAGOAS             = 'AL',
        AMAPA               = 'AP',
        AMAZONAS            = 'AM',
        BAHIA               = 'BA',
        CEARA               = 'CE',
        DISTRITO_FEDERAL    = 'DF',
        ESPIRITO_SANTO      = 'ES',
        GOIAS               = 'GO',
        MARANHAO            = 'MA',
        MATO_GROSSO         = 'MT',
        MATO_GROSSO_SUL     = 'MS',
        MINAS_GERAIS        = 'MG',
        PARA                = 'PA',
        PARAIBA             = 'PB',
        PARANA              = 'PR',
        PERNAMBUCO          = 'PE',
        PIAUI               = 'PI',
        RIO_DE_JANEIRO      = 'RJ',
        RIO_GRANDE_NORTE    = 'RN',
        RIO_GRANDE_SUL      = 'RS',
        RONDONIA            = 'RO',
        RORAIMA             = 'RR',
        SANTA_CATARINA      = 'SC',
        SAO_PAULO           = 'SP',
        SERGIPE             = 'SE',
        TOCANTINS           = 'TO';

    /**
     * @return array
     */
    public static function getAllEstados(): array
    {
        return [
            'Acre' => self::ACRE,
            'Alagoas' => self::ALAGOAS,
            'Amapá' => self::AMAPA,
            'Amazonas' => self::AMAZONAS,
            'Bahia' => self::BAHIA,
            'Ceará' => self::CEARA,
            'Distrito Federal' => self::DISTRITO_FEDERAL,
            'Espírito Santo' => self::ESPIRITO_SANTO,
            'Goiás' => self::GOIAS,
            'Maranhão' => self::MARANHAO,
            'Mato Grosso' => self::MATO_GROSSO,
            'Mato Grosso do Sul' => self::MATO_GROSSO_SUL,
            'Minas Gerais' => self::MINAS_GERAIS,
            'Pará' => self::PARA,
            'Paraíba' => self::PARAIBA,
            'Paraná' => self::PARANA,
            'Pernambuco' => self::PERNAMBUCO,
            'Piauí' => self::PIAUI,
            'Rio de Janeiro' => self::RIO_DE_JANEIRO,
            'Rio Grande do Norte' => self::RIO_GRANDE_NORTE,
            'Rio Grande do Sul' => self::RIO_GRANDE_SUL,
            'Rondônia' => self::RONDONIA,
            'Roraima' => self::RORAIMA,
            'Santa Catarina' => self::SANTA_CATARINA,
            'São Paulo' => self::SAO_PAULO,
            'Sergipe' => self::SERGIPE,
            'Tocantins' => self::TOCANTINS
        ]; 
    }
}