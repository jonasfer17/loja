<?php

namespace App\Infrastructure\Enum;

/**
 * Class MensagemEnum
 */
class MensagemEnum
{
    public const MENSAGEM_ERRO_PADRAO = 'Ocorreu um erro.',
        MENSAGEM_ERRO_NAO_AUTORIZADO = 'Você não tem permissão para realizar essa ação.'
    ;
}