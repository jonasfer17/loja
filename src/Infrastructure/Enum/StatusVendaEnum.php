<?php

namespace App\Infrastructure\Enum;

/**
 * Class StatusVendaEnum
 * @package App\Infrastructure\Enum
 */
class StatusVendaEnum
{
    public const ABERTA = 1,
        AGUARDANDO_PAGAMENTO = 2,
        EM_ANALISE = 3,
        PAGA = 4,
        CANCELADA = 5;
}