<?php

namespace App\Infrastructure\Enum;

/**
 * Class StatusCarrinhoEnum
 * @package App\Infrastructure\Enum
 */
class StatusCarrinhoEnum
{
    public const ABERTO = 1,
        FECHADO = 2;
}