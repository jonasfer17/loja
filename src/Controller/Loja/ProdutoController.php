<?php


namespace App\Controller\Loja;

use App\Domain\Model\Produto\Produto;
use App\Infrastructure\Service\ProdutoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @Route(path="/produto")
 */
class ProdutoController extends AbstractController
{
    /**
     * @var ProdutoService
     */
    private $produtoService;

    /**
     * ProdutoController constructor.
     * @param ProdutoService $produtoService
     */
    public function __construct(ProdutoService $produtoService)
    {
        $this->produtoService = $produtoService;
    }

    /**
     * @Route("/", name="home_produto")
     */
    public function index()
    {
        return $this->render('loja/produto/index.html.twig', [
            'produtos' => $this->produtoService->getProdutos()
        ]);
    }

    /**
     * @Route(
     *     path="/info/{produto}",
     *     name="home_info_produto"
     * )
     */
    public function infoProduto(Request $request, Produto $produto)
    {
        return $this->render('loja/produto/info.html.twig', [
            'produtosRelacionados' => $this->produtoService->getProdutosRelacionados(),
            'produto' => $produto
        ]);
    }
}