<?php

namespace App\Controller\Loja;

use App\Domain\Dto\CheckoutDto;
use App\Domain\Model\Financeiro\Venda;
use App\Domain\Model\Loja\Carrinho;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Form\Usuario\LoginType;
use App\Infrastructure\Form\Venda\CheckoutType;
use App\Infrastructure\Service\CarrinhoService;
use App\Infrastructure\Service\Integracao\PagSeguroService;
use App\Infrastructure\Service\ProdutoService;
use App\Infrastructure\Service\SessionService;
use App\Infrastructure\Service\VendaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CheckoutController
 * @package App\Controller\Checkout
 *
 * @Route(path="/checkout")
 */
class CheckoutController extends AbstractController
{
    /**
     * @var CarrinhoService
     */
    private $carrinhoService;

    /**
     * @var ProdutoService
     */
    private $produtoService;

    /**
     * @var SessionService
     */
    private $sessionService;

    /**
     * @var VendaService
     */
    private $vendaService;

    /**
     * CheckoutController constructor.
     * @param CarrinhoService $carrinhoService
     * @param ProdutoService $produtoService
     * @param SessionService $sessionService
     */
    public function __construct(
        CarrinhoService $carrinhoService,
        ProdutoService $produtoService,
        SessionService $sessionService,
        VendaService $vendaService
    ) {
        $this->produtoService = $produtoService;
        $this->carrinhoService = $carrinhoService;
        $this->sessionService = $sessionService;
        $this->vendaService = $vendaService;
    }

    /**
     * @Route(path="/{carrinho}", name="home_checkout_index")
     * @param Carrinho $carrinho
     * @return Response
     */
    public function index(Carrinho $carrinho): Response
    {
        if ($this->sessionService->getSession($this->sessionService::CARRINHO_SESSION)) {
            $this->carrinhoService->atribuirCarrinho();
        }

        $carrinho = $this->carrinhoService->getCarrinhoAberto();
        return $this->render('loja/checkout/index.html.twig', [
            'formLogin' => $this->createForm(LoginType::class)->createView(),
            'venda' => $this->vendaService->getVendaByCarrinho($carrinho),
            'carrinho' => $carrinho,
            'produtosRelacionados' => $this->produtoService->getProdutosRelacionados(),
            'sub_total' => $this->carrinhoService->getValorTotalCarrinho($carrinho),
            'form' => $this->createForm(CheckoutType::class)->createView()
        ]);
    }

    /**
     * @Route(path="/finalizar/{venda}", name="home_checkout_finalizar")
     * @param Request $request
     * @param Venda $venda
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function finalizar(Request $request, Venda $venda)
    {
        try {
            $checkout = $this->createForm(CheckoutType::class, new CheckoutDto());
            $checkout->handleRequest($request);

            $response = $this->vendaService->checkout($venda, $checkout->getData());

            return $this->redirect(PagSeguroService::URL_CHECKOUT . $response);
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_dashboard');
    }

}