<?php

namespace App\Controller\Loja;

use App\Domain\Model\Loja\Carrinho;
use App\Domain\Model\Loja\CarrinhoProduto;
use App\Domain\Model\Produto\Produto;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Service\CarrinhoService;
use App\Infrastructure\Service\MovimentacaoService;
use App\Infrastructure\Service\ProdutoService;
use App\Infrastructure\Utils\FloatConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class CarrinhoController
 * @package App\Controller\Loja
 *
 * @Route(path="/carrinho")
 */
class CarrinhoController extends AbstractController
{
    /**
     * @var ProdutoService
     */
    private $produtoService;

    /**
     * @var CarrinhoService
     */
    private $carrinhoService;

    /**
     * @var MovimentacaoService
     */
    private $movimentacaoService;

    /**
     * CarrinhoController constructor.
     * @param ProdutoService $produtoService
     * @param CarrinhoService $carrinhoService
     * @param MovimentacaoService $movimentacaoService
     */
    public function __construct(
        ProdutoService $produtoService,
        CarrinhoService $carrinhoService,
        MovimentacaoService $movimentacaoService
    ) {
        $this->produtoService = $produtoService;
        $this->carrinhoService = $carrinhoService;
        $this->movimentacaoService = $movimentacaoService;
    }

    /**
     * @Route(path="/", name="home_carrinho_index")
     */
    public function index()
    {
        $carrinho = $this->carrinhoService->getCarrinhoAberto();

        return $this->render('loja/carrinho/index.html.twig', [
            'carrinho' => $carrinho,
            'produtosRelacionados' => $this->produtoService->getProdutosRelacionados(),
            'sub_total' => $this->carrinhoService->getValorTotalCarrinho($carrinho)
        ]);
    }

    /**
     * @Route(
     *     path="/add/{produto}",
     *     name="home_carrinho_add",
     *     requirements={"produto"="\d+"},
     *     methods={"GET"}
     * )
     * @param Request $request
     * @param Produto $produto
     * @return JsonResponse
     */
    public function add(Request $request, Produto $produto)
    {
        try {
            $this->carrinhoService->add($produto, $this->getUser());
            return $this->json([], Response::HTTP_CREATED);
        } catch (\Throwable $exception) {
            return $this->json([], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route(
     *     path="/update/{produto}/{quantidade}",
     *     name="home_carrinho_produto_update",
     *     requirements={"produto"="\d+", "quantidade"="\d+"},
     *     methods={"GET"}
     * )
     * @param Request $request
     * @param CarrinhoProduto $produto
     * @param int $quantidade
     * @return JsonResponse
     */
    public function updateItem(
        Request $request,
        CarrinhoProduto $produto,
        int $quantidade
    ) {
        try {
            $this->carrinhoService->updateItem($produto, $quantidade);
            $totalProduto = ($produto->getProduto()->getValorVenda() * $quantidade);

            return $this->json(
                [
                    'total_produto' => (string) FloatConverter::revert($totalProduto),
                    'total_carrinho' =>
                        (string) FloatConverter::revert(
                            $this->carrinhoService->getValorTotalCarrinho($produto->getCarrinho())
                        )
                ],
                Response::HTTP_OK
            );
        } catch (\Throwable $exception) {
            return $this->json([MensagemEnum::MENSAGEM_ERRO_PADRAO], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route(
     *     path="/finalizar/{carrinho}",
     *     name="home_carrinho_produto_finalizar",
     *     requirements={"carrinho"="\d+"},
     *     methods={"GET"}
     * )
     * @param Request $request
     * @param Carrinho $carrinho
     * @return RedirectResponse
     */
    public function finalizar(Request $request, Carrinho $carrinho)
    {
        try {
            $this->carrinhoService->finalizar($carrinho);

            return $this->redirectToRoute('home_checkout_index', ['carrinho' => $carrinho->getId()]);
        } catch (\Throwable $exception) {
            return $this->json([MensagemEnum::MENSAGEM_ERRO_PADRAO], Response::HTTP_BAD_REQUEST);
        }
    }
}