<?php

namespace App\Controller\Loja;

use App\Domain\Model\Produto\Produto;
use App\Infrastructure\Service\HomeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 */
class HomeController extends AbstractController
{
    /**
     * @var HomeService
     */
    private $homeService;

    /**
     * HomeController constructor.
     * @param HomeService $homeService
     */
    public function __construct(
        HomeService $homeService
    ) {
        $this->homeService = $homeService;
    }

    /**
     * @Route("/", name="home_index")
     */
    public function index()
    {
        return $this->render('loja/home/index.html.twig', [
            'produtosNovos' => $this->homeService->getProdutosNovos()
        ]);
    }


    /**
     * @Route(path="/contato", name="home_contato")
     */
    public function contato()
    {
        return $this->render('loja/contato.html.twig');
    }
}
