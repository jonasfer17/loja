<?php

namespace App\Controller\Admin\Configuracao;

use App\Infrastructure\Form\EmpresaType;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Service\EmpresaService;
use DomainException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     path="/admin/configuracao/empresa"
 * )
 */
class EmpresaController extends AbstractController
{
    /**
     * @var EmpresaService
     */
    private $empresaService;

    /**
     * @param EmpresaService $empresaService
     * 
     * @return void
     */
    public function __construct(EmpresaService $empresaService)
    {
        $this->empresaService = $empresaService;
    }
    
    /**
     * @Route(
     *     path="", 
     *     name="admin_configuracao_empresa",
     *     methods={"GET"}
     * )
     */
    public function index()
    {
        return $this->render('admin/configuracao/empresa/index.html.twig', [
            'form' => $this->createForm(
                EmpresaType::class,
                $this->empresaService->getDadosEmpresaPrincipal()
            )->createView()
        ]);
    }

    /**
     * @Route(
     *     path="", 
     *     name="admin_configuracao_empresa_edit",
     *     methods={"POST"}
     * )
     */
    public function edit(Request $request)
    {
        try {
            $form = $this->createForm(EmpresaType::class, $this->empresaService->getDadosEmpresaPrincipal());
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->empresaService->save($form->getData());
                
                $this->addFlash('success', 'Empresa editada com sucesso');
                return $this->redirectToRoute('admin_configuracao_empresa');
            }
        } catch (DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );  
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );   
        }

        return $this->redirectToRoute(
            'admin_configuracao_empresa'
        );
    }
}
