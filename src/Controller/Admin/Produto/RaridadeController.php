<?php

namespace App\Controller\Admin\Produto;

use App\Domain\Model\Produto\Colecao;
use App\Domain\Model\Produto\Raridade;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Form\Produto\ColecaoType;
use App\Infrastructure\Form\Produto\RaridadeType;
use App\Infrastructure\Service\ColecaoService;
use App\Infrastructure\Service\RaridadeService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route(
 *     path="/admin/produto/raridade"
 * )
 */
class RaridadeController extends AbstractController
{
    /**
     * @var RaridadeService
     */
    private $raridadeService;

    /**
     * RaridadeController constructor.
     * @param RaridadeService $raridadeService
     */
    public function __construct(RaridadeService $raridadeService)
    {
        $this->raridadeService = $raridadeService;
    }

    /**
     * @Route(
     *     path="/",
     *     name="admin_produto_raridade",
     *     methods={"GET"}
     * )
     * @return Response
     * @throws \Exception
     */
    public function index()
    {
        return $this->render('admin/produto/raridade/index.html.twig', [
            'raridades' => $this->raridadeService->getAll(),
        ]);
    }

    /**
     * @Route(
     *     path="/form/{id}",
     *     name="admin_produto_raridade_form",
     *     requirements={"id"="\d+"}
     * )
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse|Response
     */
    public function form(Request $request, int $id = null)
    {
        try {
            $raridade = null;

            if ($id) {
                $raridade = $this->raridadeService->find($id);
            }

            return $this->render('admin/produto/raridade/form.html.twig', [
                'form' => $this->createForm(
                    RaridadeType::class,
                    $raridade
                )->createView(),
                'raridade' => $raridade
            ]);
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_produto_raridade');
    }

    /**
     * @Route(
     *     path="/create",
     *     name="admin_produto_raridade_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request)
    {
        try {
            $form = $this->createForm(RaridadeType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->raridadeService->create($form->getData());

                $this->get('session')->clear();
                $this->addFlash('success', 'Raridade cadastrada com sucesso');
                return $this->redirectToRoute('admin_produto_raridade');

            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        $this->get('session')->set('form', $form->getData());
        return $this->redirectToRoute('admin_produto_raridade_form');
    }

    /**
     * @Route(
     *     path="/edit/{raridade}",
     *     requirements={"raridade"="\d+"},
     *     name="admin_produto_raridade_edit",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param Raridade $raridade
     * @return RedirectResponse
     */
    public function edit(Request $request, Raridade $raridade) {
        try {
            $form = $this->createForm(RaridadeType::class, $raridade);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->raridadeService->update($form->getData());

                $this->get('session')->clear();
                $this->addFlash('success', 'Raridade editadada com sucesso');
                return $this->redirectToRoute('admin_produto_raridade');
            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute(
            'admin_produto_raridade_form',
            [
                'id' => $raridade->getId()
            ]
        );
    }

    /**
     * @Route(
     *     path="/delete/{raridade}",
     *     requirements={"raridade"="\d+"},
     *     name="admin_produto_raridade_delete",
     *     methods={"DELETE"}
     * )
     * @param Request $request
     * @param Raridade $raridade
     * @return JsonResponse
     */
    public function delete(Request $request, Raridade $raridade)
    {
        try {
            $this->raridadeService->delete($raridade);
            return $this->json(
                'Coleção deletada com sucesso',
                Response::HTTP_OK
            );
        } catch (\DomainException $exception) {
            $mensagem = $exception->getMessage();
        } catch (ForeignKeyConstraintViolationException $foreignKeyException) {
            $mensagem = 'Há outros registros ligados a esse, para deletar deverá desvincular todos os registros';
        } catch (\Throwable $exception) {
            $mensagem = MensagemEnum::MENSAGEM_ERRO_PADRAO;
        }

        return $this->json($mensagem, Response::HTTP_BAD_REQUEST);
    }
}