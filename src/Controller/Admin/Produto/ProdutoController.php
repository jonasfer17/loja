<?php

namespace App\Controller\Admin\Produto;

use App\Domain\Model\Produto\Produto;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Form\Produto\ProdutoType;
use App\Infrastructure\Service\ProdutoService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     path="admin/produto"
 * )
 * Class ProdutoController
 * @package App\Controller\Admin\Produto
 */
class ProdutoController extends AbstractController
{
    /**
     * @var ProdutoService
     */
    private $produtoService;

    /**
     * ProdutoController constructor.
     * @param ProdutoService $produtoService
     */
    public function __construct(ProdutoService $produtoService)
    {
        $this->produtoService = $produtoService;
    }

    /**
     * @Route(
     *     path="/",
     *     name="admin_produto_item"
     * )
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index()
    {
        try {
            return $this->render('admin/produto/index.html.twig',[
                'produtos' => $this->produtoService->getAll()
            ]);
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_dashboard');
    }

    /**
     * @Route(
     *     path="/form/{id}",
     *     name="admin_produto_item_form",
     *     requirements={"id"="\d+"}
     * )
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse|Response
     */
    public function form(Request $request, int $id = null)
    {
        try {
            $produto = null;

            if ($id) {
                $produto = $this->produtoService->getDadosProduto($id);
            }

            return $this->render('admin/produto/form.html.twig', [
                'form' => $this->createForm(
                    ProdutoType::class,
                    $produto
                )->createView(),
                'produto' => $produto,
                'id' => $id
            ]);
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_produto_item');
    }

    /**
     * @Route(
     *     path="/create",
     *     name="admin_produto_item_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request)
    {
        try {
            $form = $this->createForm(ProdutoType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->produtoService->create($form->getData());

                $this->addFlash('success', 'Produto cadastrado com sucesso');

                return $this->redirectToRoute('admin_produto_item');
            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_produto_item_form');
    }

    /**
     * @Route(
     *     path="/edit/{produto}",
     *     name="admin_produto_item_edit",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param Produto $produto
     * @return RedirectResponse
     */
    public function edit(Request $request, Produto $produto)
    {
        try {
            $form = $this->createForm(ProdutoType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->produtoService->edit($form->getData(), $produto);

                $this->get('session')->clear();
                $this->addFlash('success', 'Produto editado com sucesso');

                return $this->redirectToRoute('admin_produto_item');
            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_produto_item');
    }

    /**
     * @Route(
     *     path="/delete/{produto}",
     *     requirements={"produto"="\d+"},
     *     name="admin_produto_item_delete",
     *     methods={"DELETE"}
     * )
     * @param Request $request
     * @param Produto $produto
     * @return JsonResponse
     */
    public function delete(Request $request, Produto $produto)
    {
        try {
            $this->produtoService->delete($produto);
            return $this->json(
                'Produto deletado com sucesso',
                Response::HTTP_OK
            );
        } catch (\DomainException $exception) {
            $mensagem = $exception->getMessage();
        } catch (ForeignKeyConstraintViolationException $foreignKeyException) {
            $mensagem = 'Há outros registros ligados a esse, para deletar deverá desvincular todos os registros';
        } catch (\Throwable $exception) {
            $mensagem = MensagemEnum::MENSAGEM_ERRO_PADRAO;
        }

        return $this->json($mensagem, Response::HTTP_BAD_REQUEST);
    }
}