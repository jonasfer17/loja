<?php

namespace App\Controller\Admin\Produto;

use App\Domain\Model\Produto\Colecao;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Form\Produto\ColecaoType;
use App\Infrastructure\Service\ColecaoService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route(
 *     path="/admin/produto/colecao"
 * )
 */
class ColecaoController extends AbstractController
{
    /**
     * @var ColecaoService
     */
    private $colecaoService;

    /**
     * ColecaoController constructor.
     * @param ColecaoService $colecaoService
     */
    public function __construct(ColecaoService $colecaoService)
    {
        $this->colecaoService = $colecaoService;
    }

    /**
     * @Route(
     *     path="/",
     *     name="admin_produto_colecao",
     *     methods={"GET"}
     * )
     * @return Response
     * @throws \Exception
     */
    public function index()
    {
        return $this->render('admin/produto/colecao/index.html.twig', [
            'colecoes' => $this->colecaoService->getAll(),
        ]);
    }

    /**
     * @Route(
     *     path="/form/{id}",
     *     name="admin_produto_colecao_form",
     *     requirements={"id"="\d+"}
     * )
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse|Response
     */
    public function form(Request $request, int $id = null)
    {
        try {
            $colecao = null;

            if ($id) {
                $colecao = $this->colecaoService->find($id);
            }

            return $this->render('admin/produto/colecao/form.html.twig', [
                'form' => $this->createForm(
                    ColecaoType::class,
                    $colecao
                )->createView(),
                'colecao' => $colecao
            ]);
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_produto_colecao');
    }

    /**
     * @Route(
     *     path="/create",
     *     name="admin_produto_colecao_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request)
    {
        try {
            $form = $this->createForm(ColecaoType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->colecaoService->create($form->getData());

                $this->get('session')->clear();
                $this->addFlash('success', 'Coleção cadastrada com sucesso');
                return $this->redirectToRoute('admin_produto_colecao');

            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        $this->get('session')->set('form', $form->getData());
        return $this->redirectToRoute('admin_produto_colecao_form');
    }

    /**
     * @Route(
     *     path="/edit/{colecao}",
     *     requirements={"colecao"="\d+"},
     *     name="admin_produto_colecao_edit",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param Colecao $colecao
     * @return RedirectResponse
     */
    public function edit(Request $request, Colecao $colecao) {
        try {
            $form = $this->createForm(ColecaoType::class, $colecao);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->colecaoService->update($form->getData());

                $this->get('session')->clear();
                $this->addFlash('success', 'Coleção editadada com sucesso');
                return $this->redirectToRoute('admin_produto_colecao');
            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute(
            'admin_produto_colecao_form',
            [
                'id' => $colecao->getId()
            ]
        );
    }

    /**
     * @Route(
     *     path="/delete/{colecao}",
     *     requirements={"colecao"="\d+"},
     *     name="admin_produto_colecao_delete",
     *     methods={"DELETE"}
     * )
     * @param Request $request
     * @param Colecao $colecao
     * @return JsonResponse
     */
    public function delete(Request $request, Colecao $colecao)
    {
        try {
            $this->colecaoService->delete($colecao);
            return $this->json(
                'Coleção deletada com sucesso',
                Response::HTTP_OK
            );
        } catch (\DomainException $exception) {
            $mensagem = $exception->getMessage();
        } catch (ForeignKeyConstraintViolationException $foreignKeyException) {
            $mensagem = 'Há outros registros ligados a esse, para deletar deverá desvincular todos os registros';
        } catch (\Throwable $exception) {
            $mensagem = MensagemEnum::MENSAGEM_ERRO_PADRAO;
        }

        return $this->json($mensagem, Response::HTTP_BAD_REQUEST);
    }
}