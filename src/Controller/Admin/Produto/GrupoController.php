<?php

namespace App\Controller\Admin\Produto;

use App\Domain\Model\Produto\Grupo;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Form\Produto\GrupoType;
use App\Infrastructure\Service\GrupoProdutoService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     path="/admin/produto/grupo"
 * )
 * Class GrupoController
 * @package App\Controller\Admin\Produto
 */
class GrupoController extends AbstractController
{
    /**
     * @var GrupoProdutoService
     */
    private $grupoProdutoService;

    /**
     * GrupoController constructor.
     * @param GrupoProdutoService $grupoProdutoService
     */
    public function __construct(GrupoProdutoService $grupoProdutoService)
    {
        $this->grupoProdutoService = $grupoProdutoService;
    }

    /**
     * @Route(
     *     path="",
     *     name="admin_produto_grupo",
     *     methods={"GET"}
     * )
     * @return Response
     * @throws \Exception
     */
    public function index()
    {
        return $this->render('admin/produto/grupo/index.html.twig', [
            'grupos' => $this->grupoProdutoService->getAll(),
        ]);
    }

    /**
     * @Route(
     *     path="/form/{id}",
     *     name="admin_produto_grupo_form",
     *     requirements={"id"="\d+"}
     * )
     * @param Request $request
     * @param int|null $id
     * @return RedirectResponse|Response
     */
    public function form(Request $request, int $id = null)
    {
        try {
            $grupo = null;

            if ($id) {
                $grupo = $this->grupoProdutoService->find($id);
            }

            return $this->render('admin/produto/grupo/form.html.twig', [
                'form' => $this->createForm(
                    GrupoType::class,
                    $grupo
                )->createView(),
                'grupo' => $grupo,
                'id' => $id
            ]);
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        return $this->redirectToRoute('admin_produto_grupo');
    }

    /**
     * @Route(
     *     path="/create",
     *     name="admin_produto_grupo_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request)
    {
        try {
            $form = $this->createForm(GrupoType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->grupoProdutoService->create($form->getData());

                $this->get('session')->clear();
                $this->addFlash('success', 'Grupo de Produto cadastrado com sucesso');
                return $this->redirectToRoute('admin_produto_grupo');

            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        $this->get('session')->set('form', $form->getData());
        return $this->redirectToRoute('admin_produto_grupo_form');
    }

    /**
     * @Route(
     *     path="/edit/{grupo}",
     *     requirements={"grupo"="\d+"},
     *     name="admin_produto_grupo_edit",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param Grupo $grupo
     * @return RedirectResponse
     */
    public function edit(Request $request, Grupo $grupo) {
        try {
            $form = $this->createForm(GrupoType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->grupoProdutoService->update($form->getData(), $grupo);

                $this->get('session')->clear();
                $this->addFlash('success', 'Grupo de Produto editadado com sucesso');
                return $this->redirectToRoute('admin_produto_grupo');
            }
        } catch (\DomainException $exception) {
            $this->addFlash(
                'error',
                $exception->getMessage()
            );
        } catch (\Throwable $exception) {
            $this->addFlash(
                'error',
                MensagemEnum::MENSAGEM_ERRO_PADRAO
            );
        }

        $this->get('session')->set('form', $form->getData());
        return $this->redirectToRoute(
            'admin_produto_grupo_form',
            [
                'id' => $grupo->getId()
            ]
        );
    }

    /**
     * @Route(
     *     path="/delete/{grupo}",
     *     requirements={"grupo"="\d+"},
     *     name="admin_produto_grupo_delete",
     *     methods={"DELETE"}
     * )
     * @param Request $request
     * @param Grupo $grupo
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request, Grupo $grupo)
    {
        try {
            $this->grupoProdutoService->delete($grupo);
            return $this->json(
                'Grupo de Produto deletado com sucesso',
                Response::HTTP_OK
            );
        } catch (\DomainException $exception) {
            $mensagem = $exception->getMessage();
        } catch (ForeignKeyConstraintViolationException $foreignKeyException) {
            $mensagem = 'Há outros registros ligados a esse, para deletar deverá desvincular todos os registros';
        } catch (\Throwable $exception) {
            $mensagem = MensagemEnum::MENSAGEM_ERRO_PADRAO;
        }

        return $this->json($mensagem, Response::HTTP_BAD_REQUEST);
    }
}