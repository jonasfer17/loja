<?php

namespace App\Domain\Dto;

/**
 * Class GrupoProdutoDto
 * @package App\Domain\Dto
 */
class GrupoProdutoDto
{
    /**
     * @var string|null
     */
    private $nome;

    /**
     * @var int|null
     */
    private $grupoPai;

    /**
     * @return string|null
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string|null $nome
     */
    public function setNome(?string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return int|null
     */
    public function getGrupoPai(): ?int
    {
        return $this->grupoPai;
    }

    /**
     * @param int|null $grupoPai
     */
    public function setGrupoPai(?int $grupoPai): void
    {
        $this->grupoPai = $grupoPai;
    }
}