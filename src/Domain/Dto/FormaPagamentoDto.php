<?php

namespace App\Domain\Dto;

/**
 * Class FormaPagamentoDto
 * @package App\Domain\Dto
 */
class FormaPagamentoDto
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}