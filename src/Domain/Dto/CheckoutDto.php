<?php

namespace App\Domain\Dto;

/**
 * Class CheckoutDto
 * @package App\Domain\Dto
 */
class CheckoutDto
{
    /**
     * @var PessoaDto|null
     */
    private $pessoa;

    private $formaPagamento;

    /**
     * @var boolean|null
     */
    private $enderecoFaturamento;

    /**
     * @return PessoaDto|null
     */
    public function getPessoa(): ?PessoaDto
    {
        return $this->pessoa;
    }

    /**
     * @param PessoaDto|null $pessoaDto
     */
    public function setPessoa(?PessoaDto $pessoaDto): void
    {
        $this->pessoa = $pessoaDto;
    }

    /**
     * @return bool|null
     */
    public function getEnderecoFaturamento(): ?bool
    {
        return $this->enderecoFaturamento;
    }

    /**
     * @param bool|null $enderecoFaturamento
     */
    public function setEnderecoFaturamento(?bool $enderecoFaturamento): void
    {
        $this->enderecoFaturamento = $enderecoFaturamento;
    }

    public function getFormaPagamento()
    {
        return $this->formaPagamento;
    }

    public function setFormaPagamento($formaPagamento): void
    {
        $this->formaPagamento = $formaPagamento;
    }


}