<?php

namespace App\Domain\Dto;

/**
 * Class UsuarioDto
 */
class UsuarioDto
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $nome;
 
    /**
     * @var string
     */
    private $email;

    /**
     * @var array
     */
    private $roles = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id 
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    
    /**
     * @return  string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome 
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email 
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles 
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }
}