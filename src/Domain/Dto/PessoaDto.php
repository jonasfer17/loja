<?php

namespace App\Domain\Dto;

class PessoaDto
{
    /**
     * @var int|null
     */
    private $tipo;

    /**
     * @var string|null
     */
    private $nome;
    
     /**
     * @var string|null
     */
    private $cpfCnpj;

    /**
     * @var EnderecoDto
     */
    private $enderecoDto;
    
    /**
     * @var ContatoDto
     */
    private $contatoDto;

    public function __construct(
        ?int $tipo = null, 
        ?string $nome = null,
        ?string $cpfCnpj = null,
        ?ContatoDto $contatoDto = null,
        ?EnderecoDto $enderecoDto = null
    ) {
        $this->tipo = $tipo;
        $this->nome = $nome;
        $this->cpfCnpj = $cpfCnpj;
        $this->contatoDto = $contatoDto ?? new ContatoDto();
        $this->enderecoDto = $enderecoDto ?? new EnderecoDto();
    }

    /**
     * @return int|null
     */
    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    /**
     * @return string|null
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @return string|null
     */
    public function getCpfCnpj(): ?string
    {
        return $this->cpfCnpj;
    }

    /**
     * @return ContatoDto|null
     */
    public function getContato(): ?ContatoDto
    {
        return $this->contatoDto;
    }

    /**
     * @return EnderecoDto|null
     */
    public function getEndereco(): ?EnderecoDto    {
        return $this->enderecoDto;
    }

    /**
     * @param int|null $tipo 
     */
    public function setTipo($tipo): void
    {
        $this->tipo = $tipo;
    }

    /**
     * @param string|null $nome 
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @param string|null $cpfCnpj 
     */
    public function setCpfCnpj($cpfCnpj): void
    {
        $this->cpfCnpj = $cpfCnpj;
    }

    /**
     * @param EnderecoDto $enderecoDto 
     */
    public function setEndereco(EnderecoDto $enderecoDto): void
    {
        $this->enderecoDto = $enderecoDto;
    }

    /**
     * @param ContatoDto $contatoDto 
     */
    public function setContato(ContatoDto $contatoDto): void
    {
        $this->contatoDto = $contatoDto;
    }
}