<?php

namespace App\Domain\Dto;

/**
 * Class MenuDto
 */
class MenuDto
{
    /**
     * @var string
     */
    private $descricao;
    
    /**
     * @var string
     */
    private $icone;

    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $subMenu;

    /**
     * @var string
     */ 
    private $controller;

    /**
     * @param string $descricao
     * @param string $controller
     * @param string|null $path
     * @param array|null $subMenu
     * @param string|null $icone
     */
    public function __construct(
        string $descricao,
        string $controller,
        ?string $path,
        ?array $subMenu,
        ?string $icone
    ) {
        $this->descricao = $descricao;
        $this->path = $path;
        $this->subMenu = $subMenu;
        $this->icone = $icone;
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getDescricao(): string
    {
        return $this->descricao;
    }

    /**
     * @return string|null
     */
    public function getIcone(): ?string
    {
        return $this->icone;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @return array|null
     */
    public function getSubMenu(): ?array
    {
        return $this->subMenu;
    }

    /**
     * @return mixed
     */
    public function getController(): string
    {
        return $this->controller;
    }
}