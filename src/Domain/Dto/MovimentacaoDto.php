<?php


namespace App\Domain\Dto;

/**
 * Class MovimentacaoDto
 * @package App\Domain\Dto
 */
class MovimentacaoDto
{
    /**
     * @var string|null
     */
    private $valorCusto;

    /**
     * @var string|null
     */
    private $valorVenda;

    /**
     * @var string|null
     */
    private $quantidade;

    /**
     * @return string|null
     */
    public function getValorCusto(): ?string
    {
        return $this->valorCusto;
    }

    /**
     * @param string|null $valorCusto
     */
    public function setValorCusto(?string $valorCusto): void
    {
        $this->valorCusto = $valorCusto;
    }

    /**
     * @return string|null
     */
    public function getValorVenda(): ?string
    {
        return $this->valorVenda;
    }

    /**
     * @param string|null $valorVenda
     */
    public function setValorVenda(?string $valorVenda): void
    {
        $this->valorVenda = $valorVenda;
    }

    /**
     * @return string|null
     */
    public function getQuantidade(): ?string
    {
        return $this->quantidade;
    }

    /**
     * @param string|null $estoqueAtual
     */
    public function setQuantidade(?string $quantidade): void
    {
        $this->quantidade = $quantidade;
    }
}