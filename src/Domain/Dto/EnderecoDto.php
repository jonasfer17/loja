<?php

namespace App\Domain\Dto;

/**
 * Class EnderecoDto
 */
class EnderecoDto
{
    /**
     * @var string|null
     */
    private $rua;

    /**
     * @var int|null
     */
    private $numero;

     /**
     * @var string|null
     */
    private $cep;

    /**
     * @var string|null
     */
    private $complemento;

    /**
     * @var string|null
     */
    private $bairro;

    /**
     * @var string|null
     */
    private $municipio;

     /**
     * @var string|null
     */
    private $estado;

    /**
     * @var string|null
     */
    private $pais;

    public function __construct(
        ?string $rua = null,
        ?int $numero = null,
        ?string $cep = null,
        ?string $bairro = null,
        ?string $municipio = null,
        ?string $estado = null
    ) {
        $this->rua = $rua;
        $this->numero = $numero;
        $this->cep = $cep;
        $this->bairro = $bairro;
        $this->municipio = $municipio;
        $this->estado = $estado;
    }

    /**
     * @return string|null
     */
    public function getRua(): ?string
    {
        return $this->rua;
    }

    /**
     * @return int|null
     */
    public function getNumero(): ?int
    {
        return $this->numero;
    }

    /**
     * @return string|null
     */
    public function getCep(): ?string
    {
        return $this->cep;
    }

    /**
     * @return string|null
     */
    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    /**
     * @return string|null
     */
    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    /**
     * @return string|null
     */
    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    /**
     * @return string|null
     */
    public function getEstado(): ?string
    {
        return $this->estado;
    }

    /**
     * @return string|null
     */
    public function getPais(): ?string
    {
        return $this->pais;
    }

    /**
     * @param string|null $rua 
     */
    public function setRua($rua): void
    {
        $this->rua = $rua;
    }

    /**
     * @param int|null $numero 
     */
    public function setNumero($numero): void
    {
        $this->numero = $numero;
    }

    /**
     * @param string|null $cep 
     */
    public function setCep($cep): void
    {
        $this->cep = $cep;
    }

    /**
     * @param string|null $bairro 
     */
    public function setBairro($bairro): void
    {
        $this->bairro = $bairro;
    }

    /**
     * @param string|null $municipio 
     */
    public function setMunicipio($municipio): void
    {
        $this->municipio = $municipio;
    }

    /**
     * @param string|null $estado 
     */
    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @param string|null $pais 
     */
    public function setPais($pais): void
    {
        $this->pais = $pais;
    }

    /**
     * @param string|null $complemento
     */
    public function setComplemento(?string $complemento): void
    {
        $this->complemento = $complemento;
    }

}