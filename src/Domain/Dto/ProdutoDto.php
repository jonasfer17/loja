<?php

namespace App\Domain\Dto;


/**
 * Class ProdutoDto
 * @package App\Domain\Dto
 */
class ProdutoDto
{
    /**
     * @var int|null
     */
    private $grupo;

    /**
     * @var int|null
     */
    private $colecao;

    /**
     * @var int|null
     */
    private $raridade;

    /**
     * @var string|null
     */
    private $idioma;

    /**
     * @var string|null
     */
    private $nome;

    /**
     * @var MovimentacaoDto|null
     */
    private $movimentacao;

    /**
     * @var string
     */
    private $descricao;

    /**
     * @return int|null
     */
    public function getGrupo(): ?int
    {
        return $this->grupo;
    }

    /**
     * @param int|null $grupo
     */
    public function setGrupo(?int $grupo): void
    {
        $this->grupo = $grupo;
    }

    /**
     * @return int|null
     */
    public function getColecao(): ?int
    {
        return $this->colecao;
    }

    /**
     * @param int|null $colecao
     */
    public function setColecao(?int $colecao): void
    {
        $this->colecao = $colecao;
    }

    /**
     * @return int|null
     */
    public function getRaridade(): ?int
    {
        return $this->raridade;
    }

    /**
     * @param int|null $raridade
     */
    public function setRaridade(?int $raridade): void
    {
        $this->raridade = $raridade;
    }

    /**
     * @return string|null
     */
    public function getIdioma(): ?string
    {
        return $this->idioma;
    }

    /**
     * @param string|null $idioma
     */
    public function setIdioma(?string $idioma): void
    {
        $this->idioma = $idioma;
    }

    /**
     * @return string|null
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string|null $nome
     */
    public function setNome(?string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return MovimentacaoDto|null
     */
    public function getMovimentacao(): ?MovimentacaoDto
    {
        return $this->movimentacao;
    }

    /**
     * @param MovimentacaoDto|null $movimentacao
     */
    public function setMovimentacao(?MovimentacaoDto $movimentacao): void
    {
        $this->movimentacao = $movimentacao;
    }

    /**
     * @return string|null
     */
    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    /**
     * @param string|null $descricao
     */
    public function setDescricao(?string $descricao): void
    {
        $this->descricao = $descricao;
    }
}