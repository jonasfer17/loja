<?php

namespace App\Domain\Dto;

/**
 * Class EmpresaDto
 */
class EmpresaDto
{
    /**
     * @var PessoaDto
     */
    private $pessoa;

    public function __construct(
        ?PessoaDto $pessoaDto = null
    ) {
        $this->pessoa = $pessoaDto ?? new PessoaDto();
    }

    /**
     * @return PessoaDto|null
     */
    public function getPessoa(): ?PessoaDto
    {
        return $this->pessoa;
    }
}