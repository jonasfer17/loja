<?php

namespace App\Domain\Dto;

/**
 * Class ContatoDto
 */
class ContatoDto
{
     /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $telefone;

     /**
     * @var string|null
     */
    private $celular;

    /**
     * @param string|null $email
     * @param string|null $telefone
     * @param string|null $celular
     */
    public function __construct(
        ?string $email = null,
        ?string $telefone = null,
        ?string $celular = null
    ) {
        $this->email = $email;
        $this->telefone = $telefone;
        $this->celular = $celular;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

     /**
     * @return string|null
     */
    public function getCelular(): ?string
    {
        return $this->celular;
    }

    /**
     * @param string|null $email 
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @param string|null $celular 
     */
    public function setCelular(?string $celular): void
    {
        $this->celular = $celular;
    }

    /**
     * @param string|null $telefone 
     */
    public function setTelefone(?string $telefone): void
    {
        $this->telefone = $telefone;
    }

}