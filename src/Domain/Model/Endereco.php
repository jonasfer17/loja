<?php

namespace App\Domain\Model;

use App\Domain\Model\Timestamps;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\EnderecoRepository")
 */
class Endereco
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Ator", inversedBy="endereco")
     * @ORM\JoinColumn(
     *     name="id_ator",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $ator;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $rua;

    /**
     * @var int|null
     * 
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numero;

     /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $municipio;

     /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $pais;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id 
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Ator
     */
    public function getAtor(): Ator
    {
        return $this->ator;
    }

    /**
     * @param Ator $ator
     */
    public function setAtor(Ator $ator): void
    {
        $this->ator = $ator;
    }

    /**
     * @return string|null
     */
    public function getRua(): ?string
    {
        return $this->rua;
    }

    /**
     * @param string|null $rua 
     */
    public function setRua(?string $rua): void
    {
        $this->rua = $rua;
    }

    /**
     * @return int|null
     */
    public function getNumero(): ?int
    {
        return $this->numero;
    }

    /**
     * @param int|null $numero 
     */
    public function setNumero(?int $numero): void
    {
        $this->numero = $numero;
    }

    /**
     * @return string|null
     */
    public function getCep(): ?string
    {
        return $this->cep;
    }

    /**
     * @param string|null $cep 
     */
    public function setCep(?string $cep): void
    {
        $this->cep = $cep;
    }

    /**
     * @return string|null
     */
    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    /**
     * @param string|null $bairro 
     */
    public function setBairro(?string $bairro): void
    {
        $this->bairro = $bairro;
    }

    /**
     * @return string|null
     */
    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    /**
     * @param string|null $municipio 
     */
    public function setMunicipio(?string $municipio): void
    {
        $this->municipio = $municipio;
    }

    /**
     * @return string|null
     */
    public function getEstado(): ?string
    {
        return $this->estado;
    }

    /**
     * @param string|null $estado 
     */
    public function setEstado(?string $estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @return string|null
     */
    public function getPais(): ?string
    {
        return $this->pais;
    }

    /**
     * @param string|null $pais 
     */
    public function setPais(?string $pais): void
    {
        $this->pais = $pais;
    }
}
