<?php

namespace App\Domain\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\AtorRepository")
 * @ORM\Table(name="ator")
 */
class Ator
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * 
     * @ORM\Column(type="integer", nullable=false)
     */
    private $tipo;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    private $nome;

    /**
     * @var Collection
     * 
     * @ORM\OneToMany(
     *     targetEntity="App\Domain\Model\Contato",
     *     mappedBy="ator",
     *     cascade={"persist", "remove"}
     * )
     */
    private $contato;

    /**
     * @var Collection
     * 
     * @ORM\OneToMany(
     *     targetEntity="App\Domain\Model\Endereco",
     *     mappedBy="ator",
     *     cascade={"persist", "remove"}
     * )
     */
    private $endereco;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $cpfCnpj;

    public function __construct()
    {
        $this->contato = new ArrayCollection();
        $this->endereco = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

        /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * @param string $nome 
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return int
     */
    public function getTipo(): int
    {
        return $this->tipo;
    }

    /**
     * @param int $tipo 
     */
    public function setTipo(int $tipo): void
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string|null
     */
    public function getCpfCnpj(): ?string
    {
        return $this->cpfCnpj;
    }

    /**
     * @param string|null $cpfCnpj 
     */
    public function setCpfCnpj(?string $cpfCnpj): void
    {
        $this->cpfCnpj = $cpfCnpj;
    }

    /**
     * @return Collection
     */
    public function getContato(): ?Collection
    {
        return $this->contato;
    }

    /**
     * @param Collection $contato 
     */
    public function setContato(?Collection $contato): void
    {
        $this->contato = $contato;
    }

    /**
     * @return Collection|null
     */
    public function getEndereco(): ?Collection
    {
        return $this->endereco;
    }

    /**
     * @param Collection
     */
    public function setEndereco(Collection $endereco): void
    {
        $this->endereco = $endereco;
    }
}
