<?php

namespace App\Domain\Model;

use App\Domain\Model\Timestamps;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\ContatoRepository")
 */
class Contato
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Ator
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Ator", inversedBy="contato")
     * @ORM\JoinColumn(
     *     name="id_ator",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $ator;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefone;

     /**
     * @var string|null
     * 
     * @ORM\Column(type="string", nullable=true)
     */
    private $celular;

    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id 
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Ator
     */
    public function getAtor(): Ator
    {
        return $this->ator;
    }

    /**
     * @param Ator $ator 
     */
    public function setAtor(Ator $ator): void
    {
        $this->ator = $ator;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email 
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getTelefone(): ?string
    {
        return $this->numero;
    }

    /**
     * @param string|null $telefone 
     */
    public function setTelefone(?int $telefone): void
    {
        $this->telefone = $telefone;
    }

    /**
     * @return string|null
     */
    public function getCelular(): ?string
    {
        return $this->celular;
    }

    /**
     * @param string|null $celular 
     */
    public function setCelular(?string $celular): void
    {
        $this->celular = $celular;
    }
}
