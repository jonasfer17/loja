<?php

namespace App\Domain\Model\Loja;

use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Timestamps;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CarrinhoProduto
 * @package App\Domain\Model\Loja
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\Loja\CarrinhoProdutoRepository")
 */
class CarrinhoProduto
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Carrinho
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Loja\Carrinho", inversedBy="produtosCarrinho")
     * @ORM\JoinColumn(
     *     name="id_carrinho",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $carrinho;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Produto\Produto")
     * @ORM\JoinColumn(
     *     name="id_produto",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $produto;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Carrinho
     */
    public function getCarrinho(): Carrinho
    {
        return $this->carrinho;
    }

    /**
     * @param Carrinho $carrinho
     */
    public function setCarrinho(Carrinho $carrinho): void
    {
        $this->carrinho = $carrinho;
    }

    /**
     * @return Produto
     */
    public function getProduto(): Produto
    {
        return $this->produto;
    }

    /**
     * @param Produto $produto
     */
    public function setProduto(Produto $produto): void
    {
        $this->produto = $produto;
    }

    /**
     * @return int|null
     */
    public function getQuantidade(): ?int
    {
        return $this->quantidade;
    }

    /**
     * @param int|null $quantidade
     */
    public function setQuantidade(?int $quantidade): void
    {
        $this->quantidade = $quantidade;
    }
}