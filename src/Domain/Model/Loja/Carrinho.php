<?php

namespace App\Domain\Model\Loja;

use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Timestamps;
use App\Domain\Model\Usuario;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\Loja\CarrinhoRepository")
 */
class Carrinho
{
    use Timestamps;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $hash;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Usuario")
     * @ORM\JoinColumn(
     *     name="id_usuario",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $usuario;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Domain\Model\Loja\CarrinhoProduto",
     *     mappedBy="carrinho",
     *     cascade={"persist", "remove"}
     * )
     */
    private $produtosCarrinho;

    /**
     * @var int|null
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $status;

    public function __construct()
    {
        $this->produtosCarrinho = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return Usuario
     */
    public function getUsuario(): Usuario
    {
        return $this->usuario;
    }

    /**
     * @param Usuario $usuario
     */
    public function setUsuario(Usuario $usuario): void
    {
        $this->usuario = $usuario;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Collection
     */
    public function getProdutosCarrinho(): Collection
    {
        return $this->produtosCarrinho;
    }

    /**
     * @param CarrinhoProduto $carrinhoProduto
     */
    public function addProdutoCarrinho(CarrinhoProduto $carrinhoProduto): void
    {
        $this->produtosCarrinho->add($carrinhoProduto);
    }
}
