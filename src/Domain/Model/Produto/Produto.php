<?php

namespace App\Domain\Model\Produto;

use App\Domain\Model\Timestamps;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Validation;

/**
 * @ORM\Entity()
 */
class Produto
{
    use Timestamps;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $nome;

    /**
     * @var Grupo
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Produto\Grupo")
     * @ORM\JoinColumn(
     *     name="id_grupo",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $grupo;

    /**
     * @var Raridade
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Produto\Raridade")
     * @ORM\JoinColumn(
     *     name="id_raridade",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $raridade;

    /**
     * @var Colecao
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Produto\Colecao")
     * @ORM\JoinColumn(
     *     name="id_colecao",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $colecao;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $idioma;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorVenda;

    /**
     * @var float|null
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorCompra;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $descricao;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return Grupo|null
     */
    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    /**
     * @param Grupo|null $grupo
     */
    public function setGrupo(?Grupo $grupo): void
    {
        $this->grupo = $grupo;
    }

    /**
     * @return Raridade|null
     */
    public function getRaridade(): ?Raridade
    {
        return $this->raridade;
    }

    /**
     * @param Raridade|null $raridade
     */
    public function setRaridade(?Raridade $raridade): void
    {
        $this->raridade = $raridade;
    }

    /**
     * @return Colecao|null
     */
    public function getColecao(): ?Colecao
    {
        return $this->colecao;
    }

    /**
     * @param Colecao|null $colecao
     */
    public function setColecao(?Colecao $colecao): void
    {
        $this->colecao = $colecao;
    }

    /**
     * @return string|null
     */
    public function getIdioma(): ?string
    {
        return $this->idioma;
    }

    /**
     * @param string|null $idioma
     */
    public function setIdioma(?string $idioma): void
    {
        $this->idioma = $idioma;
    }

    /**
     * @return float
     */
    public function getValorVenda(): float
    {
        return $this->valorVenda;
    }

    /**
     * @param float $valorVenda
     */
    public function setValorVenda(float $valorVenda): void
    {
        $this->valorVenda = $valorVenda;
    }

    /**
     * @return float|null
     */
    public function getValorCompra(): ?float
    {
        return $this->valorCompra;
    }

    /**
     * @param float|null $valorCompra
     */
    public function setValorCompra(?float $valorCompra): void
    {
        $this->valorCompra = $valorCompra;
    }

    /**
     * @return string|null
     */
    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    /**
     * @param string|null $descricao
     */
    public function setDescricao(?string $descricao): void
    {
        $this->descricao = $descricao;
    }
}