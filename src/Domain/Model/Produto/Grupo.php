<?php

namespace App\Domain\Model\Produto;

use App\Domain\Model\Timestamps;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\Produto\GrupoRepository")
 * @ORM\Table(name="produto_grupo")
 */
class Grupo
{
    use Timestamps;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $nome;

    /**
     * @var Grupo
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Produto\Grupo")
     * @ORM\JoinColumn(
     *     name="id_grupo_pai",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $grupoPai;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return Grupo|null
     */
    public function getGrupoPai(): ?Grupo
    {
        return $this->grupoPai;
    }

    /**
     * @param Grupo|null $grupoPai
     */
    public function setGrupoPai(?Grupo $grupoPai): void
    {
        $this->grupoPai = $grupoPai;
    }
}