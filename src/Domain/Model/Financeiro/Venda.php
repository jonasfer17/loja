<?php

namespace App\Domain\Model\Financeiro;

use App\Domain\Model\Loja\Carrinho;
use App\Domain\Model\Timestamps;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Venda
 * @package App\Domain\Model\Financeiro
 * @ORM\Entity()
 */
class Venda
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Carrinho
     *
     * @ORM\OneToOne(targetEntity="App\Domain\Model\Loja\Carrinho")
     * @ORM\JoinColumn(
     *     name="id_carrinho",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $carrinho;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorTotal;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Carrinho
     */
    public function getCarrinho(): Carrinho
    {
        return $this->carrinho;
    }

    /**
     * @param Carrinho $carrinho
     */
    public function setCarrinho(Carrinho $carrinho): void
    {
        $this->carrinho = $carrinho;
    }

    /**
     * @return float
     */
    public function getValorTotal(): float
    {
        return $this->valorTotal;
    }

    /**
     * @param float $valorTotal
     */
    public function setValorTotal(float $valorTotal): void
    {
        $this->valorTotal = $valorTotal;
    }

    /**
     * @return int
     */
    public function getFormaPagamento(): int
    {
        return $this->formaPagamento;
    }

    /**
     * @param int $formaPagamento
     */
    public function setFormaPagamento(int $formaPagamento): void
    {
        $this->formaPagamento = $formaPagamento;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}