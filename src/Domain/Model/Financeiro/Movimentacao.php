<?php

namespace App\Domain\Model\Financeiro;

use App\Domain\Model\Produto\Produto;
use App\Domain\Model\Timestamps;
use App\Domain\Model\Usuario;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Movimentacao
{
    use Timestamps;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Produto\Produto")
     * @ORM\JoinColumn(
     *     name="id_produto",
     *     referencedColumnName="id"
     * )
     */
    private $produto;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Usuario")
     * @ORM\JoinColumn(
     *     name="id_usuario",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $usuario;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=false)
     */
    private $quantidade;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=false)
     */
    private $ajuste;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $descricao;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorVenda;

    /**
     * @var float|null
     *
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorCompra;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Produto
     */
    public function getProduto(): Produto
    {
        return $this->produto;
    }

    /**
     * @param Produto $produto
     */
    public function setProduto(Produto $produto): void
    {
        $this->produto = $produto;
    }

    /**
     * @return Usuario
     */
    public function getUsuario(): Usuario
    {
        return $this->usuario;
    }

    /**
     * @param Usuario $usuario
     */
    public function setUsuario(Usuario $usuario): void
    {
        $this->usuario = $usuario;
    }

    /**
     * @return float
     */
    public function getQuantidade(): float
    {
        return $this->quantidade;
    }

    /**
     * @param float $quantidade
     */
    public function setQuantidade(float $quantidade): void
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return float
     */
    public function getAjuste(): float
    {
        return $this->ajuste;
    }

    /**
     * @param float $ajuste
     */
    public function setAjuste(float $ajuste): void
    {
        $this->ajuste = $ajuste;
    }

    /**
     * @return string
     */
    public function getDescricao(): string
    {
        return $this->descricao;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao(string $descricao): void
    {
        $this->descricao = $descricao;
    }

    /**
     * @return float
     */
    public function getValorVenda(): float
    {
        return $this->valorVenda;
    }

    /**
     * @param float $valorVenda
     */
    public function setValorVenda(float $valorVenda): void
    {
        $this->valorVenda = $valorVenda;
    }

    /**
     * @return float|null
     */
    public function getValorCompra(): ?float
    {
        return $this->valorCompra;
    }

    /**
     * @param float|null $valorCompra
     */
    public function setValorCompra(?float $valorCompra): void
    {
        $this->valorCompra = $valorCompra;
    }
}